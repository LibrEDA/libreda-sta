// SPDX-FileCopyrightText: 2021-2024 Thomas Kramer <code@tkramer.ch>
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Lazy shortest-path computation for a single source and arbitrary destinations.

use std::collections::BinaryHeap;
use std::hash::Hash;

use fnv::FnvHashMap;
use petgraph::visit::GraphBase;
use petgraph::{
    algo::Measure,
    visit::{EdgeRef, IntoEdges, VisitMap, Visitable},
};

use super::Ordered;

pub struct Lazy;
pub struct Precomputed;

/// Lazy shortest-path search.
/// Can also be used for longest-path search in directed acyclic graphs
/// by flipping the sign of the edge costs.
pub struct Dijkstra<Type, G, D, F>
where
    G: GraphBase + Visitable,
{
    edge_cost: F,
    graph: G,
    start: G::NodeId,
    predecessor_map: FnvHashMap<G::NodeId, NextHop<D, G::NodeId>>,
    /// Wavefront
    heap: BinaryHeap<Ordered<D, G::NodeId>>,
    visited: <G as Visitable>::Map,
    /// If the graph contains negative weights (= longest path search)
    /// then we can't do lazy search.
    allow_negative_weights: bool,
    zero: D,
    _type: std::marker::PhantomData<Type>,
}

impl<T, G, D, F> Clone for Dijkstra<T, G, D, F>
where
    G: GraphBase + Visitable + Clone,
    G::Map: Clone,
    F: Clone,
    D: Clone,
{
    fn clone(&self) -> Self {
        Self {
            edge_cost: self.edge_cost.clone(),
            graph: self.graph.clone(),
            start: self.start,
            predecessor_map: self.predecessor_map.clone(),
            heap: self.heap.clone(),
            visited: self.visited.clone(),
            allow_negative_weights: self.allow_negative_weights,
            zero: self.zero.clone(),
            _type: Default::default(),
        }
    }
}
impl<G, D, F> Dijkstra<Lazy, G, D, F>
where
    G: GraphBase + Visitable + IntoEdges,
    D: Measure,
    F: Fn(G::EdgeRef) -> D,
{
    pub fn new(graph: G, start: G::NodeId, edge_cost: F) -> Dijkstra<Lazy, G, D, F> {
        Dijkstra {
            visited: graph.visit_map(),
            graph,
            edge_cost,
            start,
            predecessor_map: Default::default(),
            heap: Default::default(),
            allow_negative_weights: false,
            zero: D::default(),
            _type: Default::default(),
        }
    }
}

impl<G, D, F> Dijkstra<Precomputed, G, D, F>
where
    G: IntoEdges + Visitable,
    G::NodeId: Eq + Hash,
    F: Fn(G::EdgeRef) -> D,
    D: Measure + Copy,
{
    /// Compute next hop along the shortest path to the source node.
    pub fn next_hop(&self, dest: &G::NodeId) -> Option<NextHop<D, G::NodeId>> {
        self.next_hop_cached(dest)
    }
}

impl<G, D, F> Dijkstra<Lazy, G, D, F>
where
    G: IntoEdges + Visitable,
    G::NodeId: Eq + Hash,
    F: Fn(G::EdgeRef) -> D,
    D: Measure + Copy,
{
    /// Compute next hop along the shortest path to the source node.
    pub fn next_hop(&mut self, dest: &G::NodeId) -> Option<NextHop<D, G::NodeId>> {
        self.next_hop_cached(dest)
            .or_else(|| self.search_next_hop(dest))
    }

    /// Precompute the next hop for all nodes connected to the start node.
    pub fn precompute(mut self) -> Dijkstra<Precomputed, G, D, F> {
        self.allow_negative_weights = true;
        let n = self.start;
        self.search_next_hop(&n);

        Dijkstra {
            visited: self.visited,
            graph: self.graph,
            start: self.start,
            edge_cost: self.edge_cost,
            predecessor_map: self.predecessor_map,
            heap: Default::default(),
            allow_negative_weights: true,
            zero: self.zero,
            _type: Default::default(),
        }
    }
}

impl<T, G, D, F> Dijkstra<T, G, D, F>
where
    G: IntoEdges + Visitable,
    G::NodeId: Eq + Hash,
    F: Fn(G::EdgeRef) -> D,
    D: Measure + Copy,
{
    /// Allow negative weights.
    pub fn allow_negative_weights(self, allow: bool) -> Self {
        Self {
            allow_negative_weights: allow,
            ..self
        }
    }

    /// Simple lookup in cache. Returns `None` if the shortest path to this node is not computed yet.
    pub fn next_hop_cached(&self, dest: &G::NodeId) -> Option<NextHop<D, G::NodeId>> {
        self.predecessor_map.get(dest).copied()
    }

    /// Do path search until `dest` is found.
    fn search_next_hop(&mut self, dest: &G::NodeId) -> Option<NextHop<D, G::NodeId>> {
        debug_assert!(self.allow_negative_weights || !self.visited.is_visited(dest));
        if self.predecessor_map.is_empty() {
            // Insert start node.
            self.heap.push(Ordered(D::default(), self.start));
        }
        while let Some(Ordered(dist, a)) = self.heap.pop() {
            if !self.allow_negative_weights && self.visited.is_visited(&a) {
                continue;
            }
            self.visited.visit(a);

            for e in self.graph.edges(a) {
                let next_node = e.target();
                let weight = (self.edge_cost)(e);
                if weight < self.zero {
                    debug_assert!(self.allow_negative_weights, "graph has negative weights");
                }
                let new_dist = dist + weight;

                use std::collections::hash_map::Entry;

                let next_hop = NextHop {
                    distance_to_source: new_dist,
                    next_hop: a,
                    edge_cost: weight,
                };

                match self.predecessor_map.entry(next_node) {
                    Entry::Occupied(entry) => {
                        if new_dist < entry.get().distance_to_source {
                            // Update shortest distance.
                            *entry.into_mut() = next_hop;
                        }
                    }
                    Entry::Vacant(entry) => {
                        entry.insert(next_hop);
                        // Expand search from new node.
                        self.heap.push(Ordered(new_dist, next_node));
                    }
                };
            }
            if !self.allow_negative_weights && &a == dest {
                // Arrived at destination.
                // If all weights are positive, this means we found the shortest path.
                break;
            }
        }
        self.next_hop_cached(dest)
    }
}

#[test]
fn test_lazy_dijkstra() {
    use petgraph::stable_graph::StableDiGraph;

    let mut g = StableDiGraph::new();
    let a = g.add_node("a");
    let b = g.add_node("b");
    let c = g.add_node("c");
    let d = g.add_node("d");
    let e = g.add_node("e");
    let f = g.add_node("f");

    g.add_edge(a, b, 1i32);
    g.add_edge(a, c, 2);
    g.add_edge(b, d, 1);
    g.add_edge(c, d, 1);
    g.add_edge(b, e, 1);
    g.add_edge(c, e, 1);
    g.add_edge(e, f, 1);
    g.add_edge(d, f, 2);

    let mut dij = Dijkstra::new(&g, a, |e| *e.weight());

    assert_eq!(dij.next_hop(&b).unwrap().next_hop, a);
    assert_eq!(dij.next_hop(&c).unwrap().next_hop, a);
    assert_eq!(dij.next_hop(&d).unwrap().next_hop, b);
    assert_eq!(dij.next_hop(&e).unwrap().next_hop, b);
    assert_eq!(dij.next_hop(&f).unwrap().next_hop, e);
    assert_eq!(dij.next_hop(&b).unwrap().distance_to_source, 1);
    assert_eq!(dij.next_hop(&c).unwrap().distance_to_source, 2);
    assert_eq!(dij.next_hop(&d).unwrap().distance_to_source, 2);
    assert_eq!(dij.next_hop(&e).unwrap().distance_to_source, 2);
    assert_eq!(dij.next_hop(&f).unwrap().distance_to_source, 3);
}

pub fn dijkstra<G, F, D>(
    graph: G,
    start: G::NodeId,
    edge_cost: F,
) -> FnvHashMap<G::NodeId, NextHop<D, G::NodeId>>
where
    G: IntoEdges + Visitable,
    G::NodeId: Eq + Hash,
    F: Fn(G::EdgeRef) -> D,
    D: Measure + Copy,
{
    let mut heap = BinaryHeap::new();
    let mut predecessor_map: FnvHashMap<_, NextHop<D, G::NodeId>> = Default::default();
    let mut visited = graph.visit_map();

    // Insert start node.
    heap.push(Ordered(D::default(), start));

    while let Some(Ordered(dist, a)) = heap.pop() {
        if visited.is_visited(&a) {
            continue;
        }
        visited.visit(a);

        for e in graph.edges(a) {
            let next_node = e.target();
            let weight = edge_cost(e);
            let new_dist = dist + weight;

            use std::collections::hash_map::Entry;
            match predecessor_map.entry(next_node) {
                Entry::Occupied(entry) => {
                    if new_dist < entry.get().distance_to_source {
                        // Update shortest distance.
                        *entry.into_mut() = NextHop {
                            distance_to_source: new_dist,
                            next_hop: a,
                            edge_cost: weight,
                        };
                    }
                }
                Entry::Vacant(entry) => {
                    entry.insert(NextHop {
                        distance_to_source: new_dist,
                        next_hop: a,
                        edge_cost: weight,
                    });
                    // Expand search from new node.
                    heap.push(Ordered(new_dist, next_node));
                }
            };
        }
    }

    predecessor_map
}

#[derive(Copy, Clone, Debug)]
pub struct NextHop<D, N> {
    pub distance_to_source: D,
    /// Next hop along the critical path towards the source node.
    pub next_hop: N,
    /// Edge cost to the `next_hop`.
    pub edge_cost: D,
}

#[test]
fn test_dijkstra_digraph() {
    use petgraph::stable_graph::StableDiGraph;

    let mut g = StableDiGraph::new();
    let a = g.add_node("a");
    let b = g.add_node("b");
    let c = g.add_node("c");
    let d = g.add_node("d");
    let e = g.add_node("e");
    let f = g.add_node("f");

    g.add_edge(a, b, 1i32);
    g.add_edge(a, c, 2);
    g.add_edge(b, d, 1);
    g.add_edge(c, d, 1);
    g.add_edge(b, e, 1);
    g.add_edge(c, e, 1);
    g.add_edge(e, f, 1);
    g.add_edge(d, f, 2);

    let distances = dijkstra(&g, a, |e| *e.weight());

    assert_eq!(distances[&b].next_hop, a);
    assert_eq!(distances[&c].next_hop, a);
    assert_eq!(distances[&d].next_hop, b);
    assert_eq!(distances[&e].next_hop, b);
    assert_eq!(distances[&f].next_hop, e);
    assert_eq!(distances[&b].distance_to_source, 1);
    assert_eq!(distances[&c].distance_to_source, 2);
    assert_eq!(distances[&d].distance_to_source, 2);
    assert_eq!(distances[&e].distance_to_source, 2);
    assert_eq!(distances[&f].distance_to_source, 3);
}

#[test]
fn test_dijkstra_digraph_negative_weights() {
    use petgraph::stable_graph::StableDiGraph;

    let mut g = StableDiGraph::new();
    let [src, dest, via1, via2] = ["src", "dest", "via1", "via2"].map(|n| g.add_node(n));

    g.add_edge(src, dest, -2);
    g.add_edge(src, via1, -1);
    g.add_edge(via1, via2, -1);
    g.add_edge(via2, dest, -1);

    let distances = dijkstra(&g, src, |e| *e.weight());

    assert_eq!(distances[&dest].next_hop, via2);
}
#[test]
fn test_lazy_dijkstra_digraph_negative_weights() {
    use petgraph::stable_graph::StableDiGraph;

    let mut g = StableDiGraph::new();
    let [src, dest, via1, via2] = ["src", "dest", "via1", "via2"].map(|n| g.add_node(n));

    g.add_edge(src, dest, -2);
    g.add_edge(src, via1, -1);
    g.add_edge(via1, via2, -1);
    g.add_edge(via2, dest, -1);

    let mut distances = Dijkstra::new(&g, src, |e| *e.weight()).allow_negative_weights(true);

    assert_eq!(distances.next_hop(&dest).unwrap().next_hop, via2);
}
