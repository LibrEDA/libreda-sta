// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Base trait for cell delay and constraint models.

use libreda_db::prelude::NetlistIds;

use blanket::blanket;

/// Base trait for cell delay and constraint models.
#[blanket(derive(Ref))]
pub trait CellModel<N: NetlistIds> {
    /// Get the cell pins in a consistent ordering.
    /// The same ordering will be used for passing input signals to the cell delay model
    /// or cell constraint model.
    fn ordered_pins(&self, cell: &N::CellId) -> Vec<N::PinId>;
}
