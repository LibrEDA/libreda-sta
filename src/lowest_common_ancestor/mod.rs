// SPDX-FileCopyrightText: 2024 Thomas Kramer <code@tkramer.ch>
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Algorithm for finding the lowest common ancestor (LCA) of two
//! nodes in a directed graph.
//!
//! # References
//! * M. A. Bender and M. F. Colton, “The LCA problem revisited”, [Proceedings of the 4th Latin American Symposium on Theoretical Informatics](https://archive.org/details/latin2000theoret0000lati/page/88)
//! * Alternative algorithm: Dash, Santanu Kumar; Scholz, Sven-Bodo; Herhut, Stephan; Christianson, Bruce (2013), "A scalable approach to computing representative lowest common ancestor in directed acyclic graphs", [PDF](http://uhra.herts.ac.uk/bitstream/2299/12152/2/906535.pdf)

use std::collections::HashMap;
use std::hash::Hash;

use petgraph::visit::{GraphBase, IntoEdgesDirected};
use petgraph::Direction;

mod argmin_lut;
use argmin_lut::ArgMinRange;

/// Efficient lookup of lowest-common ancestor nodes in a tree.
pub struct LowestCommonAncestor<G>
where
    G: GraphBase,
{
    euler_tour: ArgMinRange<Vec<EulerNode<G::NodeId>>, EulerNode<G::NodeId>>,
    first_occurrence: HashMap<G::NodeId, usize>,
}

/// A node in the euler tour.
#[derive(Debug, Copy, Clone, Hash)]
struct EulerNode<T> {
    /// Node name.
    node: T,
    /// Level of the node.
    level: usize,
}

impl<T> EulerNode<T> {
    pub fn new(node: T, level: usize) -> Self {
        Self { node, level }
    }
}

impl<T> Eq for EulerNode<T> {}

impl<T> PartialEq for EulerNode<T> {
    fn eq(&self, other: &Self) -> bool {
        self.level == other.level
    }
}

impl<T> PartialOrd for EulerNode<T> {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.level.partial_cmp(&other.level)
    }
}

impl<T> Ord for EulerNode<T> {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.level.cmp(&other.level)
    }
}

impl<G> LowestCommonAncestor<G>
where
    G: IntoEdgesDirected,
    G::NodeId: Eq + Hash,
{
    /// Create a new lowest common ancestor lookup table.
    /// The root of the tree is assumed to have only outgoing edges.
    /// If the root of the tree has only incoming edges, consider using `new_with_direction()`.
    pub fn new(graph: G, root: G::NodeId) -> Result<Self, ()> {
        Self::new_with_direction(graph, root, Direction::Outgoing)
    }

    /// Same as `new()` but allows to specify the edge direction in the tree.
    pub fn new_with_direction(
        graph: G,
        root: G::NodeId,
        direction_at_root: Direction,
    ) -> Result<Self, ()> {
        let (euler_tour, first_occurrence) =
            levelized_euler_tour_tree(&graph, root, direction_at_root)?;

        // Precompute lookup-table for fast search of minimum value.
        let euler_tour = ArgMinRange::new(euler_tour);

        Ok(Self {
            euler_tour,
            first_occurrence,
        })
    }

    /// Find the lowest common ancestor node of `a` and `b`.
    /// # Panics
    /// Panics if a or b is not in the graph or not connected to the root node.
    pub fn lowest_common_ancestor(&self, a: &G::NodeId, b: &G::NodeId) -> &G::NodeId {
        // The lowest common ancestor is at the smallest level between the first occurrence of a
        // and first occurrence of b.

        let first_a = self.first_occurrence[a];
        let first_b = self.first_occurrence[b];

        // Sort indices.
        let (first_a, first_b) = if first_a < first_b {
            (first_a, first_b)
        } else {
            (first_b, first_a)
        };

        let min_level_node = self.euler_tour.min(first_a, first_b + 1 - first_a);
        &min_level_node.node
    }
}

#[test]
fn test_lowest_common_ancestor_trivial() {
    use petgraph::stable_graph::StableDiGraph;
    let mut g: StableDiGraph<_, ()> = StableDiGraph::new();
    let root = g.add_node("root");

    let lca = LowestCommonAncestor::new(&g, root).unwrap();

    assert_eq!(lca.lowest_common_ancestor(&root, &root), &root);
}

#[test]
fn test_lowest_common_ancestor() {
    use petgraph::stable_graph::StableDiGraph;
    let mut g = StableDiGraph::new();
    let root = g.add_node("root");
    let a = g.add_node("a");
    let b = g.add_node("b");
    let c = g.add_node("c");
    let d = g.add_node("d");
    g.add_edge(root, a, ());
    g.add_edge(root, d, ());
    g.add_edge(a, b, ());
    g.add_edge(a, c, ());

    let lca = LowestCommonAncestor::new(&g, root).unwrap();

    assert_eq!(lca.lowest_common_ancestor(&root, &root), &root);
    assert_eq!(lca.lowest_common_ancestor(&root, &c), &root);
    assert_eq!(lca.lowest_common_ancestor(&root, &d), &root);
    assert_eq!(lca.lowest_common_ancestor(&c, &d), &root);
    assert_eq!(lca.lowest_common_ancestor(&b, &c), &a);
    assert_eq!(lca.lowest_common_ancestor(&c, &a), &a);
}

/// Create a list of nodes in the euler tour through the tree. Associate with each node
/// its level in the tree. The root node has level 0.
///
/// *Requires* that the graph is indeed a tree, otherwise will return an `Err`.
fn levelized_euler_tour_tree<G>(
    g: G,
    root: G::NodeId,
    direction_at_root: Direction,
) -> Result<(Vec<EulerNode<G::NodeId>>, HashMap<G::NodeId, usize>), ()>
where
    G: IntoEdgesDirected,
    G::NodeId: Eq + Hash,
{
    let mut tour = vec![];
    let mut node_stack = vec![(root, g.neighbors_directed(root, direction_at_root), 0)];
    let mut first_node_occurrence = HashMap::new();
    first_node_occurrence.insert(root, 0);

    {
        // Check for potential cycles: all edges of root must be directed the same way.
        let num_wrong_edges = g.edges_directed(root, direction_at_root.opposite()).count();
        if num_wrong_edges > 0 {
            // That's not the root.
            return Err(());
        }
    }

    while let Some((n, mut neighbours, level)) = node_stack.pop() {
        tour.push(EulerNode { node: n, level });
        if let Some(next) = neighbours.next() {
            {
                // TODO: This could be skipped if we can safely assume that `g` is a tree.
                // Check for potential cycles: a non-root node must have exactly one parent node.
                let num_parents = g.edges_directed(next, direction_at_root.opposite()).count();
                if num_parents != 1 {
                    // This is not a tree.
                    return Err(());
                }
            }
            // Remember the position of the first occurrence.
            first_node_occurrence.insert(next, tour.len());
            node_stack.push((n, neighbours, level));
            node_stack.push((
                next,
                g.neighbors_directed(next, direction_at_root),
                level + 1,
            ))
        } else {
            // We visited all neighbours of n.
        }
    }

    Ok((tour, first_node_occurrence))
}

#[test]
fn test_euler_tour() {
    use petgraph::stable_graph::StableDiGraph;
    let mut g = StableDiGraph::new();
    let root = g.add_node("root");
    let a = g.add_node("a");
    let b = g.add_node("b");
    g.add_edge(a, b, ());
    g.add_edge(root, a, ());

    let (tour, first_occurrence) =
        levelized_euler_tour_tree(&g, root, Direction::Outgoing).unwrap();
    assert_eq!(
        tour,
        vec![
            EulerNode::new(root, 0),
            EulerNode::new(a, 1),
            EulerNode::new(b, 2),
            EulerNode::new(a, 1),
            EulerNode::new(root, 0)
        ]
    );

    assert_eq!(first_occurrence[&root], 0);
    assert_eq!(first_occurrence[&a], 1);
    assert_eq!(first_occurrence[&b], 2);
}

#[test]
fn test_euler_tour_with_non_tree() {
    use petgraph::stable_graph::StableDiGraph;
    let mut g = StableDiGraph::new();
    let root = g.add_node("root");
    let a = g.add_node("a");
    let b = g.add_node("b");
    let multiple_parents = g.add_node("multiple_parents");

    g.add_edge(a, b, ());
    g.add_edge(root, a, ());
    g.add_edge(a, multiple_parents, ());
    g.add_edge(b, multiple_parents, ());

    let result = levelized_euler_tour_tree(&g, root, Direction::Outgoing);
    assert_eq!(result, Err(()));
}
