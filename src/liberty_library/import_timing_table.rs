// SPDX-FileCopyrightText: 2024 Thomas Kramer <code@tkramer.ch>
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Parse and import a timing table from a liberty data structure.

use super::*;
use liberty_io::Group;

/// Read a lookup table with indices and put it into an interpolator struct
/// such that the lookup table can be evaluated like a function.
///
///
/// * `required_var1_name`: The name of the variable which should be used as first variable in the interpolated function.
/// * `required_var2_name`: The name of the variable which should be used as second variable in the interpolated function.
/// * `cell_name`: Name of the cell. Used for log output.
/// * `pin_name`: Name of the pin. Used for log output.
///
pub(super) fn get_interpolated_timing_table(
    timing_group: &Group,
    table_group: &Group,
    table_templates: &HashMap<String, LuTableTemplate>,
    required_var1_name: &str,
    required_var2_name: &str,
    cell_name: &str,
    pin_name: &str,
) -> Result<Interp, LibertyErr> {
    assert_eq!(
        timing_group.name, "timing",
        "Expected a timing group. Found: '{}'",
        timing_group.name
    );

    let template_name = table_group
        .arguments
        .first()
        .and_then(|v| v.as_str())
        .ok_or_else(|| {
            let msg = format!(
                "Timing table {} has no template name defined in cell {}, pin {}.",
                table_group.name, cell_name, pin_name
            );
            log::error!("{}", msg);
            LibertyErr::Other(msg)
        })?;

    let mut interp = get_timing_table(
        timing_group,
        &table_group.name,
        "index_1",
        "index_2",
        "values",
    )?;

    // Check the template and validate table dimensions.
    // Make sure the variables are correctly ordered.
    {
        let (num_vars,) = if template_name == "scalar" {
            (0,)
        } else {
            let template = table_templates.get(template_name).ok_or_else(|| {
                log::error!(
                    "LUT table template is used but not defined: {}",
                    template_name
                );
                let msg = format!(
                    "LUT table template is used but not defined: {}",
                    template_name
                );
                LibertyErr::Other(msg)
            })?;

            let num_vars = if template.var2.is_some() { 2 } else { 1 };

            let var_names = [Some(template.var1.as_str()), template.var2.as_deref()];

            let output_cap_var = var_names
                .iter()
                .find_position(|&&name| name == Some(required_var1_name))
                .map(|(pos, _)| pos);

            let input_transition_var = var_names
                .iter()
                .find_position(|&&name| name == Some(required_var2_name))
                .map(|(pos, _)| pos);

            // Fix variable ordering.
            match (output_cap_var, input_transition_var) {
                (Some(0), Some(1)) => {
                    debug_assert_eq!(num_vars, 2);
                } // Ordering is fine.
                (Some(1), _) | (_, Some(0)) => {
                    debug_assert!(num_vars > 0);
                    // Fix variable ordering.
                    interp = interp.swap_variables();
                }
                _ => {
                    debug_assert_eq!(num_vars, 0);
                } // Variable ordering does not matter.
            }

            (num_vars,)
        };

        let actual_num_vars = match &interp {
            Interp::Scalar(_) => 0,
            Interp::Interp1D1(_) | Interp::Interp1D2(_) => 1,
            Interp::Interp2D(_) => 2,
        };

        if num_vars != actual_num_vars {
            log::error!(
                "Table dimension mismatch. Template '{}' defines {} variables but found {}.",
                template_name,
                num_vars,
                actual_num_vars
            );
            return Err(LibertyErr::TableMalformed);
        }
    }

    Ok(interp)
}
