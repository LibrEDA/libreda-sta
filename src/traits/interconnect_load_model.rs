// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Abstraction of interconnects as electrical loads.
//! For example, the load created by an interconnect could be modelled as a lumped capacitance.

use crate::traits::LoadBase;
use libreda_db::prelude::{NetlistBase, TerminalId};

use blanket::blanket;

/// Abstraction of electrical loads represented by interconnects.
#[blanket(derive(Ref))]
pub trait InterconnectLoadModel<N: NetlistBase>: LoadBase {
    /// Get the electrical load of the interconnect as seen by the specified driving terminal.
    fn interconnect_load(&self, netlist: &N, driver_terminal: &TerminalId<N>) -> Self::Load;
}
