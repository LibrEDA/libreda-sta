// Copyright (c) 2021-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Abstraction of the cell delay computation.

use super::delay_base::DelayBase;
use crate::{traits::CellModel, RiseFall};
use libreda_core::prelude::NetlistIds;

use blanket::blanket;

/// A delay arc within a cell.
#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub struct CellDelayArc<PinId> {
    /// Input pin of the delay arc and it's signal edge polarity.
    pub input_pin: (PinId, RiseFall),
    /// Output pin of the delay arc and it's signal edge polarity.
    pub output_pin: (PinId, RiseFall),
}

// TODO: remove commented code
// /// Collection of possible output signals.
// #[derive(Clone, Copy, PartialEq, Eq, Hash, Debug, Default)]
// pub enum OutputSignals<S> {
//     #[default]
//     None,
//     Constant(Logic3),
//     Rise(S),
//     Fall(S),
//     RiseFall(S, S),
// }

// impl<S> OutputSignals<S> {
//     pub fn map<S2>(self, f: impl Fn(S) -> S2) -> OutputSignals<S2> {
//         use OutputSignals::*;
//         match self {
//             None => None,
//             Constant(c) => Constant(c),
//             Rise(s) => Rise(f(s)),
//             Fall(s) => Fall(f(s)),
//             RiseFall(s1, s2) => RiseFall(f(s1), f(s2)),
//         }
//     }

//     pub fn rise(&self) -> Option<&S> {
//         match &self {
//             Self::Rise(s) | Self::RiseFall(s, _) => Some(s),
//             _ => None,
//         }
//     }

//     pub fn fall(&self) -> Option<&S> {
//         match &self {
//             Self::Fall(s) | Self::RiseFall(_, s) => Some(s),
//             _ => None,
//         }
//     }
// }

/// Define the computation of the cell delay.
/// A combinational delay from an input `i` to an output `o` should depend on
/// * what is attached to the output `o` (the load),
/// * on what signal arrives at input `i`
/// * and on the state of the other inputs.
#[blanket(derive(Ref))]
pub trait CellDelayModel<N: NetlistIds>: CellModel<N> + DelayBase {
    /// Propagate a signal from the `input_pin` to the `output_pin`.
    /// This is used as a more general form of computing the cell delay.
    /// The `input_pin` and the `output_pin` must be pins of the same cell.
    ///
    /// # Parameters
    /// `other_inputs`: Values at other input pins. If a value is not specified, this implies the default (for example 'unkown').
    ///
    /// Returns the output signal or `None` if there is no delay arc from the selected input to selected output.
    fn cell_output(
        &self,
        netlist: &N,
        arc: &CellDelayArc<N::PinId>,
        input_signal: &Self::Signal,
        output_load: &Self::Load,
        other_inputs: &impl Fn(&N::PinId) -> Option<Self::LogicValue>,
    ) -> Option<Self::Signal>;

    /// Iterate over the output pins of all delay arcs starting at `related_pin`.
    fn delay_arcs(
        &self,
        netlist: &N,
        cell_id: &N::CellId,
    ) -> impl Iterator<Item = CellDelayArc<N::PinId>> + '_;
}
