// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Abstraction for computation of electrical loads created by cell input pins.

use crate::traits::{CellModel, LoadBase, TimingBase};
use libreda_db::traits::NetlistIds;

use blanket::blanket;

/// Abstraction of electrical loads created by input pins.
#[blanket(derive(Ref))]
pub trait CellLoadModel<N: NetlistIds>: TimingBase + LoadBase + CellModel<N> {
    /// Get the load created by a pin, typically its capacitance.
    fn input_pin_load(
        &self,
        input_pin: &N::PinId,
        other_inputs: &impl Fn(&N::PinId) -> Option<Self::LogicValue>,
    ) -> Self::Load;

    /// Return a value representing no load at all (zero capacitance for example).
    fn zero(&self) -> Self::Load;
}
