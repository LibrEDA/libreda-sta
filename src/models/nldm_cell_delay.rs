// Copyright (c) 2021-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Implementation of the [`CellDelayModel`] using the non-linear delay model (NLDM)
//! with data from a liberty library.

use std::collections::HashSet;

use fnv::FnvHashMap;
use libreda_logic::logic_value::Logic3;
use num_traits::Zero;
use smallvec::SmallVec;
use uom::si::f64::{Capacitance, Time};

use crate::liberty_library::{LibertyTimingLibrary, Pin};
use crate::traits::cell_logic_model::{LogicModel, Unateness};
use crate::traits::timing_library::TimingLibrary;
use crate::{traits::*, DelayArcArg, RiseFall};
use crate::{ConstraintArcArg, StaMode};
use libreda_db::prelude::NetlistBase;
use libreda_db::traits::NetlistIds;

/// # Parameters
/// `'a`: Lifetime of the liberty library.
#[derive(Debug)]
pub struct NLDMCellModel<'a, N: NetlistIds> {
    sta_mode: StaMode,
    /// Choose between using NLDM or a constant delay.
    delay_model_type: DelayModelType,
    library: &'a LibertyTimingLibrary<N::CellId, N::PinId>,
    /// Table for quickly finding the input capacitance of a pin.
    /// Capacitance is available also in `pin_data` but access might be faster using this field.
    pin_capacitances: FnvHashMap<N::PinId, NLDMOutputLoad>,
    /// Lookup-table for pin data (capacitance, delay arcs, constraint arcs, ...).
    pin_data: FnvHashMap<N::PinId, &'a Pin<N::PinId>>,
    /// Cache the pin ordering for all the cells.
    // TODO: remove
    ordered_pins: FnvHashMap<N::CellId, Vec<N::PinId>>,

    /// Warn once about negative slew.
    warn_negative_slew: std::sync::Once,
    warn_negative_delay: std::sync::Once,
}

/// Specify what delay model to use.
/// This implementation of the NLDM delay model can also fall back to a constant delay model.
#[derive(Copy, Debug, Clone, PartialEq)]
pub enum DelayModelType {
    /// Use the same constant delay for all delay arcs.
    /// Used for example for levelization.
    Constant(Time),
    /// Use the non-linear delay model.
    NLDM,
}

/// Index type for selecting minimal or maximal values.
#[derive(Copy, Debug, Clone, PartialEq, Eq, Hash)]
pub enum MinMax {
    /// Earliest.
    Min = 0,
    /// Latest.
    Max = 1,
}

// /// Index type for selecting edge types.
// #[derive(Copy, Debug, Clone, PartialEq, Eq, Hash)]
// pub enum RiseFall {
//     /// Rising edge.
//     Rise = 0,
//     /// Falling edge.
//     Fall = 2,
// }

// impl From<RiseFall> for timing_library::RiseFall {
//     fn from(value: RiseFall) -> Self {
//         match value {
//             RiseFall::Rise => timing_library::RiseFall::Rise,
//             RiseFall::Fall => timing_library::RiseFall::Fall,
//         }
//     }
// }

/// Slew rate and edge polarity of a signal.
#[derive(Copy, Debug, Clone, PartialEq)]
pub struct NLDMSignal {
    /// Min and max transition times.
    slew: [Time; 2],
    /// Min and max arrival times.
    delay: [Time; 2],
    /// Logic value on which the signal stabilizes.
    // TODO: This is redundant and already stored in `edge_polarity`.
    logic_value: Logic3,
    /// Type of the transition.
    edge_polarity: SignalTransitionType,
}

impl NLDMSignal {
    /// Create a new signal with a given arrival time.
    /// Other values are set to defaults.
    pub fn new(slew_rate: Time, arrival_time: Time) -> Self {
        Self {
            edge_polarity: SignalTransitionType::Any,
            logic_value: Logic3::X,
            slew: [slew_rate; 2],
            delay: [arrival_time; 2],
        }
    }

    /// Set the polarity of the signal transition.
    /// Default is [`EdgePolarity::Unknown`].
    pub fn set_polarity(&mut self, edge_polarity: SignalTransitionType) {
        self.edge_polarity = edge_polarity;
    }

    /// Set the polarity of the signal transition.
    /// Default is [`EdgePolarity::Unknown`].
    pub fn with_polarity(mut self, edge_polarity: SignalTransitionType) -> Self {
        self.set_polarity(edge_polarity);
        self
    }

    /// Get the minimum/maximum slew for the rise or fall edge.
    pub fn slew(&self, minmax: MinMax) -> Time {
        self.slew[minmax as usize]
    }

    /// Get the minimum/maximum delay.
    pub fn delay(&self, minmax: MinMax) -> Time {
        self.delay[minmax as usize]
    }

    fn slew_mut(&mut self, minmax: MinMax) -> &mut Time {
        &mut self.slew[minmax as usize]
    }

    fn delay_mut(&mut self, minmax: MinMax) -> &mut Time {
        &mut self.delay[minmax as usize]
    }

    /// Set the slew rate for all corners.
    pub fn set_slew_rates(&mut self, slew_rate: Time) {
        assert!(slew_rate >= Time::zero(), "slew rate must be positive");
        self.slew = [slew_rate; 2];
    }
    /// Set the arrival times for all corners.
    pub fn set_arrival_times(&mut self, arrival_time: Time) {
        self.delay = [arrival_time; 2];
    }

    /// Set the slew rate for the specified corner.
    pub fn set_slew_rate(&mut self, corner: MinMax, slew_rate: Time) {
        *self.slew_mut(corner) = slew_rate;
    }
    /// Set the slew rate for the specified corner.
    pub fn set_arrival_time(&mut self, corner: MinMax, arrival_time: Time) {
        *self.delay_mut(corner) = arrival_time;
    }

    /// Take this new arrival time into acount and keep track of min/max arrival times.
    fn summarize_arrival_time(&mut self, arrival_time: Time) {
        self.set_arrival_time(MinMax::Min, arrival_time.min(self.delay(MinMax::Min)));
        self.set_arrival_time(MinMax::Max, arrival_time.max(self.delay(MinMax::Max)));
    }
    /// Accumulate transition time.
    fn acc_slew(&mut self, slew: Time) {
        self.set_slew_rate(MinMax::Min, slew.min(self.slew(MinMax::Min)));
        self.set_slew_rate(MinMax::Max, slew.max(self.slew(MinMax::Max)));
    }

    /// Set the logic value of the signal (after transition).
    pub fn set_logic_value(&mut self, value: Logic3) {
        self.logic_value = value;
    }
    /// Set the slew rate for all corners.
    pub fn with_slew_rates(mut self, slew_rate: Time) -> Self {
        self.set_slew_rates(slew_rate);
        self
    }
    /// Set the arrival times for all corners.
    pub fn with_arrival_times(mut self, arrival_time: Time) -> Self {
        self.set_arrival_times(arrival_time);
        self
    }

    /// Set the slew rate for the specified corner and transition type.
    pub fn with_slew_rate(mut self, corner: MinMax, slew_rate: Time) -> Self {
        self.set_slew_rate(corner, slew_rate);
        self
    }
    /// Set the slew rate for the specified corner and transition type.
    pub fn with_arrival_time(mut self, corner: MinMax, arrival_time: Time) -> Self {
        self.set_arrival_time(corner, arrival_time);
        self
    }

    /// Set the logic value of the signal (after transition).
    pub fn with_logic_value(mut self, value: Logic3) -> Self {
        self.set_logic_value(value);
        self
    }
}

impl Signal for NLDMSignal {
    type LogicValue = Logic3;

    fn logic_value(&self) -> Self::LogicValue {
        self.logic_value
    }

    fn transition_type(&self) -> SignalTransitionType {
        self.edge_polarity
    }

    fn with_transition_type(mut self, trans: SignalTransitionType) -> Self {
        self.edge_polarity = trans;
        self
    }
}

/// Difference of two arrival times.
#[derive(Copy, Debug, Clone, PartialEq)]
pub struct NLDMDelay {
    /// min-rise, min-fall, max-rise, max-fall
    delay: [Time; 2],
}

impl NLDMDelay {
    fn get(&self, minmax: MinMax) -> Time {
        self.delay[minmax as usize]
    }
}

impl Zero for NLDMDelay {
    fn zero() -> Self {
        Self {
            delay: [Zero::zero(); 2],
        }
    }

    fn is_zero(&self) -> bool {
        self.delay.iter().all(|d| d.is_zero())
    }
}
impl std::ops::Add for NLDMDelay {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        Self {
            delay: std::array::from_fn(|i| self.delay[i] + rhs.delay[i]),
        }
    }
}

/// Slew rate and edge polarity of a signal.
#[derive(Copy, Debug, Clone, PartialEq)]
pub struct NLDMRequiredSignal {
    /// Earliest arrival time such that constraints are met.
    pub earliest_arrival_time: Option<Time>,
    /// Latest arrival time such that constraints are met.
    pub latest_arrival_time: Option<Time>,
}

impl NLDMRequiredSignal {
    /// Create a required signal.
    pub fn new(earliest: Option<Time>, latest: Option<Time>) -> Self {
        Self {
            earliest_arrival_time: earliest,
            latest_arrival_time: latest,
        }
    }
}
impl RequiredSignal for NLDMRequiredSignal {}

/// Slew rate and edge polarity of a signal.
#[derive(Copy, Debug, Clone, PartialEq, Default)]
pub struct NLDMSlack {
    /// actual arrival time - earliest arrival time
    pub hold_slack: Option<Time>,
    /// latest arrival time - actual arrival time
    pub setup_slack: Option<Time>,
}

/// Load capacitance.
#[derive(Copy, Clone, Debug, Default)]
pub struct NLDMOutputLoad {
    load_capacitance_rise: Capacitance,
    load_capacitance_fall: Capacitance,
}

impl NLDMOutputLoad {
    fn get(&self, edge_type: RiseFall) -> Capacitance {
        match edge_type {
            RiseFall::Rise => self.load_capacitance_rise,
            RiseFall::Fall => self.load_capacitance_fall,
        }
    }
}

impl Zero for NLDMOutputLoad {
    fn zero() -> Self {
        Self {
            load_capacitance_rise: Zero::zero(),
            load_capacitance_fall: Zero::zero(),
        }
    }

    fn is_zero(&self) -> bool {
        self.load_capacitance_rise.is_zero() && self.load_capacitance_fall.is_zero()
    }
}

impl std::ops::Add for NLDMOutputLoad {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        Self {
            load_capacitance_rise: self.load_capacitance_rise + rhs.load_capacitance_rise,
            load_capacitance_fall: self.load_capacitance_fall + rhs.load_capacitance_fall,
        }
    }
}

impl<'a, N: NetlistBase> NLDMCellModel<'a, N> {
    /// Create a new NLDM cell model based on a liberty library.
    pub fn new(library: &'a LibertyTimingLibrary<N::CellId, N::PinId>, netlist: &N) -> Self {
        // TODO: remove
        let ordered_pins = netlist
            .each_cell()
            .map(|c| {
                let pins = netlist.each_pin_vec(&c);
                (c, pins)
            })
            .collect();

        let mut model = Self {
            sta_mode: StaMode::Early,
            library,
            pin_capacitances: Default::default(),
            pin_data: Default::default(),
            delay_model_type: DelayModelType::NLDM,
            ordered_pins,
            warn_negative_slew: std::sync::Once::new(),
            warn_negative_delay: std::sync::Once::new(),
        };

        model.init(netlist);

        model
    }

    /// Choose between NLDM and constant delays.
    pub fn set_delay_model_type(&mut self, model_type: DelayModelType) {
        self.delay_model_type = model_type
    }

    fn init(&mut self, netlist: &N) {
        self.init_pin_table(netlist)
    }

    /// Create a lookup-table for the default pin capacitances.
    fn init_pin_table(&mut self, netlist: &N) {
        for cell_id in netlist.each_cell() {
            for pin_id in netlist.each_pin(&cell_id) {
                let pin_data = self.library.get_pin(&cell_id, &pin_id);

                if let Some(pin_data) = pin_data {
                    self.pin_capacitances.insert(
                        pin_id.clone(),
                        NLDMOutputLoad {
                            load_capacitance_rise: pin_data.capacitance_rise,
                            load_capacitance_fall: pin_data.capacitance_fall,
                        },
                    );
                    self.pin_data.insert(pin_id, pin_data);
                }
            }
        }
    }
}

impl<'a, N: NetlistBase> LoadBase for NLDMCellModel<'a, N> {
    type Load = NLDMOutputLoad;

    fn sum_loads(&self, load1: &Self::Load, load2: &Self::Load) -> Self::Load {
        *load1 + *load2
    }
}

impl<'a, N: NetlistBase> TimingBase for NLDMCellModel<'a, N> {
    type Signal = NLDMSignal;
    type LogicValue = Logic3;
}

impl<'a, N: NetlistBase> DelayBase for NLDMCellModel<'a, N> {
    type Delay = NLDMDelay;

    fn summarize_delays(&self, a: &Self::Signal, b: &Self::Signal) -> Self::Signal {
        let logic_value = if a.logic_value == b.logic_value {
            a.logic_value
        } else {
            Default::default()
        };

        let reduction_fns = [Time::min, Time::max];

        let delay = std::array::from_fn(|i| (reduction_fns[i])(a.delay[i], b.delay[i]));
        let slew = std::array::from_fn(|i| (reduction_fns[i])(a.slew[i], b.slew[i]));

        use SignalTransitionType::*;
        let edge_polarity = match (a.edge_polarity, b.edge_polarity) {
            (Any, _) | (_, Any) => Any,
            (Rise, Fall) | (Fall, Rise) => Any,
            (Rise, _) | (_, Rise) => Rise,
            (Fall, _) | (_, Fall) => Fall,
            (Constant(c1), Constant(c2)) if c1 == c2 => Constant(c1),
            (Constant(_), Constant(_)) => Constant(Logic3::X),
        };

        NLDMSignal {
            edge_polarity,
            delay,
            slew,
            logic_value,
        }
    }

    fn get_delay(&self, from: &Self::Signal, to: &Self::Signal) -> Self::Delay {
        NLDMDelay {
            delay: std::array::from_fn(|i| to.delay[i] - from.delay[i]),
        }
    }
}

impl<'a, N: NetlistBase> CellModel<N> for NLDMCellModel<'a, N> {
    fn ordered_pins(&self, cell: &N::CellId) -> Vec<N::PinId> {
        self.ordered_pins
            .get(cell)
            .expect("pin ordering not cached for this cell")
            .clone()
    }
}

impl<'a, N: NetlistBase> CellLoadModel<N> for NLDMCellModel<'a, N> {
    fn input_pin_load(
        &self,
        input_pin: &N::PinId,
        _other_inputs: &impl Fn(&N::PinId) -> Option<Self::LogicValue>,
    ) -> Self::Load {
        self.pin_capacitances
            .get(input_pin)
            .cloned()
            .unwrap_or(self.zero())
    }

    fn zero(&self) -> Self::Load {
        Default::default()
    }
}

impl<'a, N: NetlistBase> CellDelayModel<N> for NLDMCellModel<'a, N> {
    fn cell_output(
        &self,
        netlist: &N,
        arc: &CellDelayArc<N::PinId>,
        input_signal: &Self::Signal,
        output_load: &Self::Load,
        _other_inputs: &impl Fn(&N::PinId) -> Option<Self::LogicValue>,
    ) -> Option<Self::Signal> {
        debug_assert!(
            netlist.parent_cell_of_pin(&arc.input_pin.0)
                == netlist.parent_cell_of_pin(&arc.output_pin.0),
            "start and end of the delay arc must belong to the same cell."
        );

        let cell = netlist.parent_cell_of_pin(&arc.input_pin.0);

        debug_assert_eq!(
            SignalTransitionType::from(arc.input_pin.1),
            input_signal.transition_type(),
            "edge type of input signal is not consistent with the timing arc"
        );

        if let SignalTransitionType::Constant(c) = input_signal.transition_type() {
            // Propagate constant.
            todo!("handle constant input");
        }

        let input_edge_polarity = match input_signal.transition_type() {
            SignalTransitionType::Constant(_) => unreachable!("should be handled above"),
            SignalTransitionType::Rise => RiseFall::Rise,
            SignalTransitionType::Fall => RiseFall::Fall,
            SignalTransitionType::Any => {
                panic!("edge polarity of signal should match the edge polarity of the graph node")
            }
        };

        assert_eq!(input_edge_polarity, arc.input_pin.1,
            "Actual edge polarity of the input signal does not match the edge polarity of the graph node.");

        // Store `(minmax, input slew, output slew)` tuples.
        let mut slew_cache: SmallVec<[_; 1]> = Default::default();
        // Store `(minmax, input slew, delay)` tuples.
        let mut delay_cache: SmallVec<[_; 1]> = Default::default();

        // Compute slew and delay for a certain edge type.
        let slew_fn = |minmax: MinMax| -> Option<Time> {
            let input_slew = input_signal.slew(minmax);
            let cached = slew_cache
                .iter()
                .find(|(s, _)| s == &input_slew)
                .map(|(_, s)| *s);
            if let Some(slew) = cached {
                return slew;
            }

            let slew = self
                .library
                .get_slew(
                    DelayArcArg { cell: &cell, arc },
                    input_slew,
                    output_load.get(arc.output_pin.1),
                )
                // Set negative slew rates to zero.
                .map(|s| {
                    if s < Time::zero() {
                        // Warn once about negative slew.
                        self.warn_negative_slew.call_once(|| {
                            log::warn!("Negative slewrate. Check the cell model.");
                        });
                    }
                    s.max(Zero::zero())
                });

            slew_cache.push((input_slew, slew));
            slew
        };

        let delay_fn = |minmax: MinMax| -> Option<Time> {
            let input_slew = input_signal.slew(minmax);
            let cached = delay_cache
                .iter()
                .find(|(s, _)| s == &input_slew)
                .map(|(_, d)| *d);
            if let Some(delay) = cached {
                return delay;
            }

            let delay = self
                .library
                .get_cell_delay(
                    DelayArcArg { cell: &cell, arc },
                    input_signal.slew(minmax),
                    output_load.get(arc.output_pin.1),
                )
                // Set negative delays to zero.
                .map(|d| {
                    if d < Time::zero() {
                        // Warn once about negative delay.
                        self.warn_negative_delay.call_once(|| {
                            log::warn!("Negative delay. Check the cell model.");
                        });
                    }
                    d.max(Zero::zero())
                });
            delay_cache.push((input_slew, delay));
            delay
        };

        let args = [MinMax::Min, MinMax::Max];

        let delays = args.map(delay_fn);
        let slews = args.map(slew_fn);

        for i in 0..2 {
            assert_eq!(
                delays[i].is_some(),
                slews[i].is_some(),
                "delay and slew must be defined both or none of them"
            );
        }

        let is_all_none = delays.iter().all(Option::is_none);

        if is_all_none {
            return Default::default();
        }

        let mut s = NLDMSignal::new(Time::zero(), Time::zero());
        s.logic_value = match arc.output_pin.1 {
            RiseFall::Rise => Logic3::H,
            RiseFall::Fall => Logic3::L,
        };
        s.edge_polarity = arc.output_pin.1.into();

        let mut slew_defined = false;
        let mut delay_defined = false;

        // Add delay to arrival time of input signal.

        // TODO: This is suspicious.
        for i in 0..delays.len() {
            if let Some(delay) = delays[i] {
                s.delay[i] = input_signal.delay[i] + delay;
                delay_defined = true;
            }
            if let Some(slew) = slews[i] {
                s.slew[i] = slew;
                slew_defined = true;
            }
        }

        if !delay_defined && !slew_defined {
            return Default::default();
        }

        debug_assert!(s.delay.iter().all(|d| d >= &Time::zero()));
        debug_assert!(s.slew.iter().all(|s| s >= &Time::zero()));
        debug_assert!(s.delay(MinMax::Max) >= s.delay(MinMax::Min));
        debug_assert!(s.slew(MinMax::Max) >= s.slew(MinMax::Min));

        Some(s)
    }

    fn delay_arcs(
        &self,
        _netlist: &N,
        cell: &N::CellId,
    ) -> impl Iterator<Item = CellDelayArc<N::PinId>> + '_ {
        // Fetch cell from library.
        let delay_arcs = &self
            .library
            .cells
            .get(cell)
            .expect("Cell not in library.")
            .delay_arcs;
        delay_arcs.into_iter().cloned()
    }

    // fn delay_arcs_from_pin(
    //     &self,
    //     netlist: &N,
    //     input_pin: &N::PinId,
    // ) -> Box<dyn Iterator<Item = N::PinId> + '_> {
    //     let cell = netlist.parent_cell_of_pin(input_pin);

    //     // Find the representation of the `input_pin` in the liberty library.
    //     let pin = self
    //         .library
    //         .cells
    //         .get(&cell)
    //         .and_then(|cell| cell.pins.get(input_pin));

    //     // Find the output pins of all delay arcs which start at the `input_pin`.
    //     // Usually, an input pin has only one delay arc. So use a small SmallVec.
    //     let output_pins: SmallVec<[_; 2]> = pin
    //         .into_iter()
    //         .flat_map(move |pin| pin.delay_arcs.keys())
    //         .cloned()
    //         .collect();

    //     Box::new(output_pins.into_iter())
    // }
}

impl<'a, N: NetlistBase> ConstraintBase for NLDMCellModel<'a, N> {
    type Constraint = ();

    type RequiredSignal = NLDMRequiredSignal;

    type Slack = NLDMSlack;

    fn summarize_constraints(
        &self,
        s1: &Self::RequiredSignal,
        s2: &Self::RequiredSignal,
    ) -> Self::RequiredSignal {
        let rs = [s1, s2];

        NLDMRequiredSignal {
            earliest_arrival_time: rs
                .iter()
                .flat_map(|s| s.earliest_arrival_time)
                .reduce(Time::min),
            latest_arrival_time: rs
                .iter()
                .flat_map(|s| s.latest_arrival_time)
                .reduce(Time::max),
        }
    }

    fn solve_delay_constraint(
        &self,
        actual_delay: &Self::Delay,
        required_output: &Self::RequiredSignal,
        _actual_input: &Self::Signal,
    ) -> Self::RequiredSignal {
        use MinMax::*;
        let d_max = actual_delay.get(Max);
        let d_min = actual_delay.get(Min);

        NLDMRequiredSignal {
            earliest_arrival_time: required_output.earliest_arrival_time.map(|t| t - d_min),
            latest_arrival_time: required_output.latest_arrival_time.map(|t| t - d_max),
        }
    }

    fn get_slack(
        &self,
        actual_signal: &Self::Signal,
        required_signal: &Self::RequiredSignal,
    ) -> Self::Slack {
        use MinMax::*;

        let aat_earliest = actual_signal.delay(Min);
        let aat_latest = actual_signal.delay(Max);

        let hold_slack = required_signal
            .earliest_arrival_time
            .map(|r| aat_earliest - r);

        let setup_slack = required_signal.latest_arrival_time.map(|r| r - aat_latest);

        NLDMSlack {
            hold_slack,
            setup_slack,
        }
    }

    fn add_clock_period(
        &self,
        mut required: Self::RequiredSignal,
        clock_period: Time,
    ) -> Self::RequiredSignal {
        if let Some(latest) = required.latest_arrival_time.as_mut() {
            *latest += clock_period;
        }

        required
    }
}

impl<'a, N: NetlistBase> CellConstraintModel<N> for NLDMCellModel<'a, N> {
    fn get_required_input(
        &self,
        netlist: &N,
        arc: &CellConstraintArc<N::PinId>,
        constrained_pin_signal: &Self::Signal,
        related_pin_signal: &Self::Signal,
        _other_inputs: &impl Fn(&N::PinId) -> Option<Self::Signal>,
        output_loads: &impl Fn(&N::PinId) -> Option<Self::Load>,
    ) -> Option<Self::RequiredSignal> {
        let cell = netlist.parent_cell_of_pin(&arc.constrained_pin.0);
        // Consistency check:
        debug_assert!(
            {
                let cell2 = netlist.parent_cell_of_pin(&arc.related_pin.0);
                cell == cell2
            },
            "Both pins of the constraint arc must belong to the same cell."
        );

        let output_load = output_loads(&arc.constrained_pin.0).unwrap_or(Zero::zero());

        let hold_time = |(constrained_slew, related_slew): (MinMax, MinMax)| -> Option<Time> {
            let constrained_pin_transition = constrained_pin_signal.slew(constrained_slew);
            let related_pin_transition = related_pin_signal.slew(related_slew);

            self.library.get_hold_constraint(
                ConstraintArcArg { cell: &cell, arc },
                related_pin_transition,
                constrained_pin_transition,
                output_load.get(arc.constrained_pin.1),
            )
        };

        let setup_time = |(constrained_slew, related_slew): (MinMax, MinMax)| -> Option<Time> {
            let constrained_pin_transition = constrained_pin_signal.slew(constrained_slew);
            let related_pin_transition = related_pin_signal.slew(related_slew);

            self.library.get_setup_constraint(
                ConstraintArcArg { cell: &cell, arc },
                related_pin_transition,
                constrained_pin_transition,
                output_load.get(arc.constrained_pin.1),
            )
        };

        // TODO: Maybe only (Max, Max) is necessary. Usually setup/hold gets shorter with steeper slopes - but is that guaranteed?
        // TODO: precompute a 'monotonic_in_slew' flag for the look-up tables. If the `monotonic_in_slew` flag is set, then is good enough to compute only for max slews.
        let slew_combinations = [
            (MinMax::Max, MinMax::Max),
            (MinMax::Min, MinMax::Min),
            (MinMax::Max, MinMax::Min),
            (MinMax::Min, MinMax::Max),
        ];

        let max_setup_time = slew_combinations
            .into_iter()
            .filter_map(setup_time)
            .reduce(Time::max);
        let max_hold_time = slew_combinations
            .into_iter()
            .filter_map(hold_time)
            .reduce(Time::max);

        dbg!(max_setup_time);
        dbg!(max_hold_time);

        if max_setup_time.is_some() || max_hold_time.is_some() {
            // Compute the arrival times which satisfy the constraint.
            use MinMax::*;

            let t_early = related_pin_signal.delay(Min);
            let t_late = related_pin_signal.delay(Max);

            let earliest_arrival_time = max_hold_time.map(|t_ho| t_late + t_ho);

            let latest_arrival_time = max_setup_time.map(|t_su| t_early - t_su);

            Some(NLDMRequiredSignal {
                earliest_arrival_time,
                latest_arrival_time,
            })
        } else {
            None // Tell the timing engine that there's no constraint.
        }
    }

    fn constraint_arcs(
        &self,
        _netlist: &N,
        cell_id: &N::CellId,
    ) -> impl Iterator<Item = CellConstraintArc<N::PinId>> + '_ {
        // Fetch cell from library.
        let cell = self
            .library
            .cells
            .get(cell_id)
            .expect("Cell not in library.");

        let constraint_arcs: HashSet<_> = cell
            .pins
            // Of all pins...
            .iter()
            // ... get all constraint arcs
            .flat_map(move |(constrained_pin, pin)| {
                let constraint_arcs = pin
                    .timing
                    .setup_arcs
                    .iter()
                    .chain(pin.timing.hold_arcs.iter());

                constraint_arcs.flat_map(|((related_pin, related_edge), arc)| {
                    [
                        (&arc.rise_constraint, RiseFall::Rise),
                        (&arc.fall_constraint, RiseFall::Fall),
                    ]
                    .into_iter()
                    .filter(|(lut, _)| lut.is_some())
                    .map(|(_, constrained_edge)| CellConstraintArc {
                        related_pin: (related_pin.clone(), *related_edge),
                        constrained_pin: (constrained_pin.clone(), constrained_edge),
                    })
                })
            })
            .collect();

        constraint_arcs.into_iter()
    }
}

impl<'a, N: NetlistBase> LogicModel<N> for NLDMCellModel<'a, N> {
    fn pin_function(
        &self,
        output_pin: &<N as NetlistIds>::PinId,
    ) -> &cell_logic_model::OutputFunction<N::PinId> {
        &self
            .pin_data
            .get(output_pin)
            .expect("pin not found")
            .output_function
    }

    fn timing_sense(
        &self,
        output_pin: &N::PinId,
        related_pin: &N::PinId,
        _other_inputs: &impl Fn(&N::PinId) -> Option<bool>,
    ) -> Unateness {
        self.pin_data
            .get(output_pin)
            .expect("pin not found")
            .timing
            .get_delay_lut(&(related_pin.clone(), RiseFall::Rise)) // TODO: avoid clone. Store timing sense unrelated of transition type.
            .expect("related pin not found")
            .timing_sense
    }
}
