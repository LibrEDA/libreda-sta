// SPDX-FileCopyrightText: 2021-2024 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Interface definitions for cell delay look-up as it is provided for example by Liberty libraries.
//! Imports liberty data using the `liberty-io` crate.

mod import;
mod import_timing_table;
mod propagation_pattern;

use fnv::FnvHashMap;
use smallvec::SmallVec;
use uom::si::{
    capacitance::farad,
    f64::{Capacitance, Time},
    time::second,
};

use self::propagation_pattern::{PropagationPattern, PropagationPatterns};

use super::liberty_util::*;
use crate::{
    traits::{
        cell_constraint_model::CellConstraintArc,
        cell_logic_model::{OutputFunction, Unateness},
        timing_library::{RiseFall, TimingLibrary},
    },
    CellDelayArc, ConstraintArcArg, DelayArcArg, SetupHold,
};
use itertools::Itertools;
use liberty_io::{
    boolean::{parse_boolean_function, BooleanExpr},
    CapacitiveLoadUnit, Group, TimeUnit,
};
use std::{collections::HashMap, hash::Hash, str::FromStr};

/// Lookup table which maps a `(capacitance, time)`-pair to a time.
type SlewLut = Interp<Time, Capacitance, Time>;
/// Lookup table which maps a `(capacitance, time)`-pair to a time.
type DelayLut = Interp<Time, Capacitance, Time>;
/// Lookup table which maps the slew of the constrained and related signal
/// to a constraint (e.g. setup or hold time).
type ConstraintLut = Interp<Time, Time, Time>;

/// Delay tables.
#[derive(Default, Clone, Debug)]
pub struct DelayArc {
    /// Timing sense/unateness of the arc.
    pub timing_sense: Unateness,
    /// Type of the timing arc.
    pub timing_type: TimingType,
    /// Lookup-table for rise transition times.
    rise_transition: Option<SlewLut>,
    /// Lookup-table for fall transition times.
    fall_transition: Option<SlewLut>,
    /// Lookup-table for rise times.
    cell_rise: Option<DelayLut>,
    /// Lookup-table for fall times.
    cell_fall: Option<DelayLut>,
}

/// Constraint tables.
#[derive(Default, Clone, Debug)]
pub struct ConstraintArc {
    /// Type of the timing arc.
    pub timing_type: TimingType,
    /// Constraint for rising edges of the constrained signal.
    pub rise_constraint: Option<ConstraintLut>,
    /// Constraint for falling edges of the constrained signal.
    pub fall_constraint: Option<ConstraintLut>,
}

/// Liberty timing type.
// TODO: Use TimingType of liberty-io crate.
#[allow(missing_docs)]
#[derive(Copy, Clone, Debug, Hash, Default)]
pub enum TimingType {
    #[default]
    Combinational,
    CombinationalRise,
    CombinationalFall,
    ThreeStateDisable,
    ThreeStateEnable,
    ThreeStateDisableRise,
    ThreeStateDisableFall,
    ThreeStateEnableRise,
    ThreeStateEnableFall,

    // // Sequential:
    /// Output pin is sensitive to rising edge of related signal.
    RisingEdge,
    /// Output pin is sensitive to falling edge of related signal.
    FallingEdge,
    /// Asynchronous set.
    Preset,
    /// Asynchronous clear/reset.
    Clear,
    /// Hold arc which is sensitive to rising edge at related pin.
    HoldRising,
    /// Hold arc which is sensitive to falling edge at related pin.
    HoldFalling,
    /// Setup arc which is sensitive to rising edge at related pin.
    SetupRising,
    /// Setup arc which is sensitive to falling edge at related pin.
    SetupFalling,
    RecoveryRising,
    RecoveryFalling,
    // SkewRising,
    // SkewFalling,
    RemovalRising,
    RemovalFalling,
}

impl DelayArc {
    /// Get the lookup table for transition times (slews).
    pub fn transition(&self, output_edge: RiseFall) -> Option<&SlewLut> {
        self.debug_sanity_check();
        match output_edge {
            RiseFall::Rise => self.rise_transition.as_ref(),
            RiseFall::Fall => self.fall_transition.as_ref(),
        }
    }
    /// Get the lookup table for delays.
    pub fn delay(&self, output_edge: RiseFall) -> Option<&DelayLut> {
        self.debug_sanity_check();
        match output_edge {
            RiseFall::Rise => self.cell_rise.as_ref(),
            RiseFall::Fall => self.cell_fall.as_ref(),
        }
    }

    /// Check that fields have consistent values.
    /// TODO: Enforce consisten values by structure.
    fn debug_sanity_check(&self) {
        // debug_assert_eq!(self.rising_edge, self.rise_transition.is_some());
        // debug_assert_eq!(self.falling_edge, self.fall_transition.is_some());
        // debug_assert_eq!(self.rising_edge, self.cell_rise.is_some());
        // debug_assert_eq!(self.falling_edge, self.cell_fall.is_some());
        // debug_assert_eq!(self.cell_fall.is_some(), self.fall_transition.is_some());
        // debug_assert_eq!(self.cell_rise.is_some(), self.rise_transition.is_some());
    }
}

/// Timing library based on a 'liberty' library.
#[derive(Clone, Debug)]
pub struct LibertyTimingLibrary<CellId, PinId> {
    table_templates: HashMap<String, LuTableTemplate>,
    time_unit: Time,
    capacitive_load_unit: Capacitance,
    /// Mapping from `(cell_name, pin_name, related_pin_name)` to the delay arc structure.
    pub(crate) cells: HashMap<CellId, Cell<PinId>>,
}

/// Timing model of a cell.
#[derive(Clone, Debug)]
pub(crate) struct Cell<PinId> {
    flip_flops: Vec<FlipFlop<PinId>>,
    latches: Vec<Latch<PinId>>,
    /// Timing information of pins.
    pub(crate) pins: FnvHashMap<PinId, Pin<PinId>>,
    cell_name: String,
    /// All possible delay arcs in this cell.
    /// This information is used to build the cell-internal edges in the timing graph.
    pub(super) delay_arcs: SmallVec<[CellDelayArc<PinId>; 4]>,
}

impl<PinId> Cell<PinId>
where
    PinId: Clone,
{
    /// Do precomputations for speeding-up the construction of the timing graph.
    pub(super) fn run_precomputations(&mut self) {
        self.precompute_delay_arcs()
    }

    fn precompute_delay_arcs(&mut self) {
        self.delay_arcs = self
            .pins
            .iter()
            .flat_map(move |(output_pin, pin)| {
                pin.timing
                    .delay_arcs
                    .iter()
                    .flat_map(move |(related_pin, delay_arc)| {
                        // Get the possible propagation patterns.
                        let prop_patterns =
                            PropagationPatterns::new(delay_arc.timing_type, delay_arc.timing_sense);
                        let related_edge = related_pin.1;

                        // Get the possible output edge types for the given input edge type.
                        let output_edges = [RiseFall::Rise, RiseFall::Fall].into_iter().filter(
                            move |&output_edge| {
                                let current_edge =
                                    PropagationPattern::from_risefall(related_edge, output_edge);
                                prop_patterns.contains(&current_edge)
                            },
                        );

                        // Assemble delay arc specification.
                        output_edges.map(|output_edge| CellDelayArc {
                            output_pin: (output_pin.clone(), output_edge),
                            input_pin: related_pin.clone(),
                        })
                    })
            })
            .collect()
    }
}

/// Define a cell-internal edge-triggered flip-flop.
/// Contains data extracted from a `ff` group of the liberty library.
#[derive(Clone, Debug)]
pub(crate) struct FlipFlop<PinId> {
    /// Name of non-inverted output.
    state_variable: String,
    /// Name of inverted output.
    state_variable_inv: String,
    next_state: BooleanExpr<PinId>,
    /// Flip-Flop samples the date when this boolean expression transitions from `false` to `true`.
    clocked_on: BooleanExpr<PinId>,
    /// Optional second clock condition for 'master-slave' flip-flops.
    clocked_on_also: Option<BooleanExpr<PinId>>,
    /// Number of storage bits.
    /// For normal flip-flops this is 1.
    /// For flip-flop banks this can be any number.
    num_bits: usize,
}

/// Define a cell-internal edge-triggered latch.
/// Contains data extracted from a `latch` group of the liberty library.
#[derive(Clone, Debug)]
pub(crate) struct Latch<PinId> {
    /// Name of the state variable of the latch.
    state_variable: String,
    /// Name of inverted output.
    state_variable_inv: String,
    /// Data which get sampled into the latch.
    data_in: BooleanExpr<PinId>,
    /// Condition for the latch to be transparent.
    enable: BooleanExpr<PinId>,
}

/// Timing information of an output pin.
#[derive(Default, Debug, Clone)]
pub struct Pin<PinId> {
    pin_name: String,
    /// Abstract logic function of output pins.
    pub(crate) output_function: OutputFunction<PinId>,
    /// Input capacitance of the pin (regardless of input transition).
    pub(crate) capacitance: Capacitance,
    pub(crate) capacitance_rise: Capacitance,
    pub(crate) capacitance_fall: Capacitance,
    /// Timing tables for this Pin.
    pub(crate) timing: PinTiming<PinId>,
}

/// Timing information of an output pin.
#[derive(Debug, Clone)]
pub struct PinTiming<PinId> {
    /// Mapping from input pin to the delay/slew values.
    delay_arcs: FnvHashMap<(PinId, RiseFall), DelayArc>,
    /// Mapping from related pin (and edge type) to hold arcs.
    pub(crate) hold_arcs: FnvHashMap<(PinId, RiseFall), ConstraintArc>,
    /// Mapping from related pin (and edge type) to setup arcs.
    pub(crate) setup_arcs: FnvHashMap<(PinId, RiseFall), ConstraintArc>,
}

impl<PinId> PinTiming<PinId>
where
    PinId: Hash + Eq,
{
    /// Find a delay arc by the related pin and the related edge type.
    pub fn get_delay_lut(&self, input_pin: &(PinId, RiseFall)) -> Option<&DelayArc> {
        self.delay_arcs.get(input_pin)
    }

    /// Find a constraint arc by the constraint type (setup/hold), the constrained edge type and the related pin.
    pub fn get_constraint_lut(
        &self,
        constraint_type: SetupHold,
        constrained_edge: RiseFall,
        related_pin: &(PinId, RiseFall),
    ) -> Option<&ConstraintLut> {
        use SetupHold::*;

        let arcs = match constraint_type {
            Hold => &self.hold_arcs,
            Setup => &self.setup_arcs,
        };

        arcs.get(related_pin)
            .and_then(|constraint_arc| match constrained_edge {
                RiseFall::Rise => constraint_arc.rise_constraint.as_ref(),
                RiseFall::Fall => constraint_arc.fall_constraint.as_ref(),
            })
    }
}

impl<PinId> Default for PinTiming<PinId> {
    fn default() -> Self {
        Self {
            delay_arcs: Default::default(),
            hold_arcs: Default::default(),
            setup_arcs: Default::default(),
        }
    }
}

/// Template for lookup-tables.
#[derive(Clone, Debug)]
struct LuTableTemplate {
    /// Name of the first variable.
    var1: String,
    /// Name of the second variable (if defined).
    var2: Option<String>,
    /// Dimension of the table.
    size: (usize, usize),
}

/// Context information used during import of a liberty library.
struct LibraryImportContext<'a, CellId, PinId> {
    cell_by_name: &'a dyn Fn(&str) -> Option<CellId>,
    pin_by_name: &'a dyn Fn(&CellId, &str) -> Option<PinId>,
}

/// Context information used during import of a cell from a liberty library.
struct CellImportContext<'a, CellId, PinId> {
    library_context: &'a LibraryImportContext<'a, CellId, PinId>,
    cell_id: &'a CellId,
    cell_name: &'a str,
}

/// Context information used during import of a pin from a liberty library.
struct PinImportContext<'a, CellId, PinId> {
    cell_context: &'a CellImportContext<'a, CellId, PinId>,
    pin_id: &'a PinId,
    pin_name: &'a str,
}

impl<CellId, PinId> LibertyTimingLibrary<CellId, PinId>
where
    CellId: Hash + Eq + Clone,
    PinId: Hash + Eq + Clone,
{
    /// Create a new timing library based on a liberty structure.
    pub fn new(
        lib: &Group,
        cell_by_name: impl Fn(&str) -> Option<CellId>,
        pin_by_name: impl Fn(&CellId, &str) -> Option<PinId>,
    ) -> Result<Self, LibertyErr> {
        let library_import_context = LibraryImportContext {
            cell_by_name: &cell_by_name,
            pin_by_name: &pin_by_name,
        };

        // Check that `lib` is really a `library` group.
        if lib.name != "library" {
            Err(LibertyErr::Other(format!(
                "Group must be a `library` group but it is `{}`",
                lib.name
            )))?;
        }

        // Create empty data structure.
        let mut l = Self {
            table_templates: Default::default(),
            cells: Default::default(),
            time_unit: Default::default(),
            capacitive_load_unit: Default::default(),
        };

        l.init_units(lib)?;

        Self::check_delay_model(lib)?;

        // Read table templates.
        l.table_templates = import::read_template_tables(lib)?;

        // Import cells from the liberty library.
        l.read_cells(lib, library_import_context)?;

        Ok(l)
    }

    /// Check that the specified delay model is supported.
    /// At the moment only "table_lookup" is supported.
    fn check_delay_model(lib: &Group) -> Result<(), LibertyErr> {
        let delay_model = lib
            .get_simple_attribute("delay_model")
            .and_then(|v| v.as_str());

        if delay_model != Some("table_lookup") {
            log::error!(
                "Delay model is not supported. Must be `table_lookup`. delay_model = {:?}",
                delay_model
            );
            Err(LibertyErr::UnsupportedDelayModel(
                delay_model.unwrap_or("<unknown>").to_string(),
            ))?;
        }

        Ok(())
    }

    /// Load units from the liberty data structure.
    fn init_units(&mut self, lib: &Group) -> Result<(), LibertyErr> {
        // Get time unit.
        {
            let time_unit_str = lib
                .get_simple_attribute("time_unit")
                .ok_or(LibertyErr::UnitNotDefined("time_unit"))?
                .as_str()
                .ok_or(LibertyErr::UnitNotDefined("time_unit is not a string"))?;

            let time_unit_sec = TimeUnit::from_str(time_unit_str)
                .map_err(|_| LibertyErr::UnitNotDefined("Failed to parse time_unit."))?;

            self.time_unit = Time::new::<second>(time_unit_sec.as_seconds());
        }

        // Get capacitance unit.
        {
            let cap_unit = lib
                .get_complex_attribute("capacitive_load_unit")
                .ok_or(LibertyErr::UnitNotDefined("capacitive_load_unit"))?;

            let c = CapacitiveLoadUnit::try_from(cap_unit.as_slice())
                .map_err(|_| LibertyErr::UnitNotDefined("capacitive_load_unit is malformed"))?;

            self.capacitive_load_unit = Capacitance::new::<farad>(c.as_farad());
        }

        Ok(())
    }
}

impl<CellId, PinId> LibertyTimingLibrary<CellId, PinId>
where
    CellId: Hash + Eq,
    PinId: Hash + Eq + Clone,
{
    /// Find a pin by the name of the cell and the name of the pin.
    pub fn get_pin(&self, cell: &CellId, output_pin: &PinId) -> Option<&Pin<PinId>> {
        self.cells.get(cell).and_then(|c| c.pins.get(output_pin))
    }

    pub(crate) fn get_delay_arc(&self, arc: DelayArcArg<CellId, PinId>) -> Option<&DelayArc> {
        self.get_pin(arc.cell, &arc.arc.output_pin.0)
            .and_then(|p| p.timing.get_delay_lut(&arc.arc.input_pin))
    }

    pub(crate) fn get_hold_lut(
        &self,
        cell: &CellId,
        arc: &CellConstraintArc<PinId>,
    ) -> Option<&ConstraintLut> {
        self.get_pin(cell, &arc.constrained_pin.0).and_then(|p| {
            p.timing
                .get_constraint_lut(SetupHold::Hold, arc.constrained_pin.1, &arc.related_pin)
        })
    }

    pub(crate) fn get_setup_lut(
        &self,
        cell: &CellId,
        arc: &CellConstraintArc<PinId>,
    ) -> Option<&ConstraintLut> {
        self.get_pin(cell, &arc.constrained_pin.0).and_then(|p| {
            p.timing
                .get_constraint_lut(SetupHold::Setup, arc.constrained_pin.1, &arc.related_pin)
        })
    }
}

fn get_cell_name(cell_group: &Group) -> Result<&str, LibertyErr> {
    let cell_name = cell_group
        .arguments
        .first()
        .and_then(|v| v.as_str())
        .ok_or_else(|| {
            let msg = "Cell group has no name argument.";
            log::error!("{}", msg);

            LibertyErr::Other(msg.to_string())
        })?;
    Ok(cell_name)
}

impl<CellId, PinId> TimingLibrary for LibertyTimingLibrary<CellId, PinId>
where
    CellId: Hash + Eq,
    PinId: Hash + Eq + Clone,
{
    type CellId = CellId;

    type PinId = PinId;

    fn get_slew(
        &self,
        arc: DelayArcArg<CellId, PinId>,
        input_slew: Time,
        output_capacitance: Capacitance,
    ) -> Option<Time> {
        let edge_polarity = arc.arc.output_pin.1;
        self.get_delay_arc(arc)
            .and_then(|d| d.transition(edge_polarity))
            .map(|f| {
                // println!("?_transition({}, {}) = {}", output_capacitance, input_slew, f.eval2d((input_slew, output_capacitance)));
                f.eval2d((output_capacitance, input_slew))
            })
    }

    fn get_cell_delay(
        &self,
        arc: DelayArcArg<CellId, PinId>,
        input_slew: Time,
        output_capacitance: Capacitance,
    ) -> Option<Time> {
        let edge_polarity = arc.arc.output_pin.1;
        self.get_delay_arc(arc)
            .and_then(|d| d.delay(edge_polarity))
            .map(|f| f.eval2d((output_capacitance, input_slew)))
    }

    fn get_hold_constraint(
        &self,
        arc: ConstraintArcArg<CellId, PinId>,
        related_pin_transition: Time,
        constrained_pin_transition: Time,
        output_load: Capacitance,
    ) -> Option<Time> {
        self.get_hold_lut(arc.cell, arc.arc)
            .map(|f| f.eval2d((related_pin_transition, constrained_pin_transition)))
    }

    fn get_setup_constraint(
        &self,
        arc: ConstraintArcArg<CellId, PinId>,
        related_pin_transition: Time,
        constrained_pin_transition: Time,
        output_load: Capacitance,
    ) -> Option<Time> {
        self.get_setup_lut(arc.cell, arc.arc)
            .map(|f| f.eval2d((related_pin_transition, constrained_pin_transition)))
    }
}

#[test]
fn test_load_timing_library_freepdk45() {
    use std::fs::File;
    use std::io::BufReader;

    // Parse the liberty file.
    let f = File::open("./tests/data/freepdk45/gscl45nm.lib").unwrap();
    let mut buf = BufReader::new(f);
    let result = liberty_io::read_liberty_bytes(&mut buf);
    let library = result.unwrap();
    assert_eq!(library.name.to_string(), "library");
    assert_eq!(library.arguments[0].to_string(), "gscl45nm");

    // Use strings as IDs for cells and pins.
    let cell_by_name = |name: &str| -> Option<String> { Some(name.into()) };
    let pin_by_name = |cell_id: &String, name: &str| -> Option<(String, String)> {
        Some((cell_id.clone(), name.into()))
    };

    // Load the timing library structure.
    let timing_library = LibertyTimingLibrary::new(&library, cell_by_name, pin_by_name).unwrap();
}

#[test]
fn test_lut_variable_ordering() {
    use crate::traits::CellDelayArc;

    // Parse the liberty file.

    let data = r#"
    library() {

      time_unit: "1ns" ;
      capacitive_load_unit (1, pf);
    
      delay_model: table_lookup;
      lu_table_template(delay_template_2x3) {
        variable_1 : total_output_net_capacitance;
        variable_2 : input_net_transition;
        index_1 ("1000.0, 1001.0");
        index_2 ("1000.0, 1001.0, 1002.0");
      }
      lu_table_template(delay_template_2x3_swapped_vars) {
        variable_1 : input_net_transition;
        variable_2 : total_output_net_capacitance;
        index_1 ("1000.0, 1001.0, 1002.0");
        index_2 ("1000.0, 1001.0");
      }
      cell(INVX1) {
        pin(Y) {
            timing() {
                related_pin: "A";
                cell_rise(delay_template_2x3) {
                    index_1: "1.0, 2.0";
                    index_2: "1.0, 2.0, 3.0";
                    values (
                        "1.0, 1.1, 1.2", \
                        "1.1, 1.3, 1.5"
                    );
                }
                cell_fall(delay_template_2x3_swapped_vars) { // Use other variable ordering!
                    index_1: "1.0, 2.0, 3.0";
                    index_2: "1.0, 2.0";
                    values (
                        "1.0, 1.1", \
                        "1.1, 1.3", \
                        "1.2, 1.5"
                    );
                }
            }
        }
      }
    }
    "#;

    let result = liberty_io::read_liberty_chars(data.chars());
    let library = result.unwrap();

    // Load the timing library structure.

    // Use strings as IDs for cells and pins.
    // Normally, this would be resolved by the netlist.
    let cell_by_name = |name: &str| -> Option<String> { Some(name.into()) };
    let pin_by_name = |cell_id: &String, name: &str| -> Option<(String, String)> {
        Some((cell_id.clone(), name.into()))
    };

    let timing_library = LibertyTimingLibrary::new(&library, cell_by_name, pin_by_name).unwrap();

    let invx1 = cell_by_name("INVX1").unwrap();
    dbg!(&timing_library);
    let arc_a_y = timing_library
        .get_delay_arc(DelayArcArg {
            cell: &invx1,
            arc: &CellDelayArc {
                input_pin: (pin_by_name(&invx1, "A").unwrap(), RiseFall::Rise),
                output_pin: (pin_by_name(&invx1, "Y").unwrap(), RiseFall::Fall),
            },
        })
        .unwrap();

    let cell_rise = arc_a_y.cell_rise.as_ref().unwrap();
    let cell_fall = arc_a_y.cell_fall.as_ref().unwrap();

    use uom::si::{capacitance::picofarad, time::nanosecond};
    let ns = Time::new::<nanosecond>;
    let pf = Capacitance::new::<picofarad>;

    assert!((cell_rise.eval2d((pf(2.0), ns(3.0))) - ns(1.5)).abs() < ns(1e-6));
    assert!((cell_rise.eval2d((pf(3.0), ns(2.0))) - ns(1.5)).abs() < ns(1e-6)); // Swapped variables
}
