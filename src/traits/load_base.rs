// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Base trait used as an abstraction of electrical loads.

use num_traits::Zero;

use blanket::blanket;

/// Base trait used as an abstraction of electrical loads.
#[blanket(derive(Ref))]
pub trait LoadBase {
    /// An electrical load. Typically a capacitance.
    type Load: Clone + std::fmt::Debug + Zero + Send + Sync;

    /// Compute the sum of two loads.
    fn sum_loads(&self, load1: &Self::Load, load2: &Self::Load) -> Self::Load;
}
