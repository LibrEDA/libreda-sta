// SPDX-FileCopyrightText: 2023 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Wrapper around scalars, 1D interpolation and 2D interpolation as
//! they are used in the liberty tables.

pub use libreda_interp::{interp1d::Interp1D, interp2d::Interp2D};
use ndarray::OwnedRepr;
use std::ops::{Add, Div, Mul, Sub};

/// Interpolated timing table with two, one or zero variables.
#[derive(Clone, Debug)]
pub enum Interp<Z = f64, X = f64, Y = X> {
    /// A scalar (constant) value.
    Scalar(Z),
    /// A one-dimensional table. Value depends only on the first variable.
    Interp1D1(Interp1D<X, Z>),
    /// A one-dimensional table. Value depends only on the second variable.
    Interp1D2(Interp1D<Y, Z>),
    /// A two-dimensional table
    Interp2D(Interp2D<X, Y, Z, OwnedRepr<Z>>),
}

impl<Z, X, Y> Interp<Z, X, Y> {
    /// Evaluate the interpolated function at `(x, y)`.
    /// For 1D functions `y` is ignored.
    /// For scalars both `x` and `y` is ignored.
    pub fn eval2d(&self, (x, y): (X, Y)) -> Z
    //where
    //    f64: Mul<Z, Output = Z>,
    //    Z: Num + Copy + Mul<f64, Output = Z>,
    where
        X: Copy + Sub<Output = X> + Div + PartialOrd,
        Y: Copy + Sub<Output = Y> + Div<Output = <X as Div>::Output> + PartialOrd,
        Z: Copy + Mul<<X as Div>::Output, Output = Z> + Add<Output = Z> + Sub<Output = Z>,
        <X as Div>::Output:
            Copy + Add<Output = <X as Div>::Output> + Sub<Output = <X as Div>::Output>,
    {
        match self {
            Self::Scalar(s) => *s,
            Self::Interp1D1(i) => i.eval(x),
            Self::Interp1D2(i) => i.eval(y),
            Self::Interp2D(i) => i.eval((x, y)),
        }
    }

    /// Swap the variables
    pub fn swap_variables(self) -> Interp<Z, Y, X>
    where
        Z: Clone,
    {
        match self {
            Interp::Scalar(s) => Interp::Scalar(s),
            Interp::Interp1D1(i) => Interp::Interp1D2(i),
            Interp::Interp1D2(i) => Interp::Interp1D1(i),
            Interp::Interp2D(i) => Interp::Interp2D(i.swap_variables()),
        }
    }

    pub fn map_values<Z2>(&self, f: impl Fn(&Z) -> Z2) -> Interp<Z2, X, Y>
    where
        X: PartialOrd + Clone,
        Y: PartialOrd + Clone,
    {
        match self {
            Interp::Scalar(s) => Interp::Scalar(f(s)),
            Interp::Interp1D1(i) => Interp::Interp1D1(i.map_values(f)),
            Interp::Interp1D2(i) => Interp::Interp1D2(i.map_values(f)),
            Interp::Interp2D(i) => Interp::Interp2D(i.map_values(f)),
        }
    }
    pub fn map_x_axis<Xnew>(self, f: impl Fn(X) -> Xnew) -> Interp<Z, Xnew, Y>
    where
        Xnew: PartialOrd,
    {
        match self {
            Interp::Scalar(s) => Interp::Scalar(s),
            Interp::Interp1D1(i) => Interp::Interp1D1(i.map_axis(f)),
            Interp::Interp1D2(i) => Interp::Interp1D2(i),
            Interp::Interp2D(i) => Interp::Interp2D(i.map_x_axis(f)),
        }
    }

    pub fn map_y_axis<Ynew>(self, f: impl Fn(Y) -> Ynew) -> Interp<Z, X, Ynew>
    where
        Ynew: PartialOrd,
    {
        match self {
            Interp::Scalar(s) => Interp::Scalar(s),
            Interp::Interp1D1(i) => Interp::Interp1D1(i),
            Interp::Interp1D2(i) => Interp::Interp1D2(i.map_axis(f)),
            Interp::Interp2D(i) => Interp::Interp2D(i.map_y_axis(f)),
        }
    }
}
