// SPDX-FileCopyrightText: 2024 Thomas Kramer <code@tkramer.ch>
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Critical path enumeration.
//!
//! # References
//! * Tsung-Wei Huang, Pei-Ci Wu, and Martin D. F. Wong, "UI-Timer: An Ultra-Fast Clock Network Pessimism Removal Algorithm"

use std::collections::BinaryHeap;
use std::hash::Hash;
use std::iter::Peekable;
use std::ops::Neg;

use itertools::Itertools;
use petgraph::algo::Measure;
use petgraph::stable_graph::NodeIndex;
use petgraph::stable_graph::StableDiGraph;
use petgraph::visit::IntoEdgeReferences;
use petgraph::visit::{EdgeRef, GraphBase, IntoEdgesDirected, Visitable};
use petgraph::Direction;

mod lazy_dijkstra;
use lazy_dijkstra::NextHop;

use self::lazy_dijkstra::Dijkstra;

/// Lazily find all paths in the `graph` from the `start` node to the `dest` node.
/// Order the paths by their ascending cost.
pub fn critical_paths<G, F, D>(
    graph: G,
    start: G::NodeId,
    dest: G::NodeId,
    edge_cost: F,
    allow_negative_weights: bool,
) -> CriticalPaths<G, F, D>
where
    G: IntoEdgesDirected + Visitable,
    G::NodeId: Eq + Hash,
    F: Fn(G::EdgeRef) -> D + Copy,
    D: Measure + Copy,
    G::Map: Clone,
{
    let shortest_path_map = Dijkstra::new(graph, start, edge_cost)
        .allow_negative_weights(allow_negative_weights)
        .precompute();
    let mut next_deviation_edges = BinaryHeap::new();
    let mut prefix_tree = StableDiGraph::new();

    // The root of the prefix tree stands for the shortest path without any deviations.
    // Initialize the node data of the root with the dummy edge `(start, start)`.
    let prefix_tree_root = prefix_tree.add_node(PrefixTreeNode::new(start, start, D::default()));
    next_deviation_edges.push(Ordered(D::default(), prefix_tree_root));

    CriticalPaths {
        graph,
        dest,
        start,
        edge_cost,
        shortest_path_map,
        prefix_tree,
        prefix_tree_root,
        next_deviation_edges,
        edge_ref_buffer: Default::default(),
    }
}

/// Lazily find all paths in the `graph` from the `start` node to the `dest` node.
/// Order the paths by their ascending cost.
/// # Panics
/// Panics when encountering a negative edge weight.
pub fn shortest_paths<G, F, D>(
    graph: G,
    start: G::NodeId,
    dest: G::NodeId,
    edge_cost: F,
) -> CriticalPaths<G, F, D>
where
    G: IntoEdgesDirected + Visitable,
    G::NodeId: Eq + Hash,
    F: Fn(G::EdgeRef) -> D + Copy,
    D: Measure + Copy,
    G::Map: Clone,
{
    critical_paths(graph, start, dest, edge_cost, false)
}

pub fn longest_paths<'f, G, F, D>(
    graph: G,
    start: G::NodeId,
    dest: G::NodeId,
    edge_cost: F,
) -> CriticalPaths<G, impl Fn(G::EdgeRef) -> D + Copy + 'f, D>
where
    G: IntoEdgesDirected + Visitable,
    G::NodeId: Eq + Hash,
    F: Fn(G::EdgeRef) -> D + Copy + 'f,
    D: Measure + Copy + Neg<Output = D>,
    G::Map: Clone,
{
    critical_paths(graph, start, dest, move |e| -edge_cost(e), true)
}

#[test]
fn test_backtrack_with_deviation_edges() {
    use itertools::Itertools;
    // Create graph of the form:
    //
    //   /-->b-->d---\
    //  /     \ ^     v
    // a       X       f
    //  \     / v     ^
    //   \-->c-->e---/
    //
    // All edges have weight 1, except ac and df which have weight 2.
    let mut g = StableDiGraph::new();
    let a = g.add_node("a");
    let b = g.add_node("b");
    let c = g.add_node("c");
    let d = g.add_node("d");
    let e = g.add_node("e");
    let f = g.add_node("f");

    let _ab = g.add_edge(a, b, 1i32);
    let _ac = g.add_edge(a, c, 2);
    let _bd = g.add_edge(b, d, 1);
    let _cd = g.add_edge(c, d, 1);
    let _be = g.add_edge(b, e, 1);
    let _ce = g.add_edge(c, e, 1);
    let _ef = g.add_edge(e, f, 1);
    let _df = g.add_edge(d, f, 2);

    // Compute all shortest paths to a.
    let distances = lazy_dijkstra::dijkstra(&g, a, |e| *e.weight());

    // No deviations from shortest path.
    let shortest_path =
        backtrack_with_deviations(|n| distances.get(n).copied(), f, [].into_iter()).collect_vec();
    assert_eq!(
        shortest_path,
        vec![(f, e, 1).into(), (e, b, 1).into(), (b, a, 1).into()]
    );

    // Take one deviation through (f, d).
    let shortest_path_fd = backtrack_with_deviations(
        |n| distances.get(n).copied(),
        f,
        [(d, f, 2).into()].into_iter(),
    )
    .collect_vec();
    assert_eq!(
        shortest_path_fd,
        vec![(f, d, 2).into(), (d, b, 1).into(), (b, a, 1).into()]
    );

    // Take one deviation through (e, c).
    let shortest_path_ec = backtrack_with_deviations(
        |n| distances.get(n).copied(),
        f,
        [(c, e, 1).into()].into_iter(),
    )
    .collect_vec();
    assert_eq!(
        shortest_path_ec,
        vec![(f, e, 1).into(), (e, c, 1).into(), (c, a, 2).into()]
    );

    // Take two deviations
    let shortest_path_fd_dc = backtrack_with_deviations(
        |n| distances.get(n).copied(),
        f,
        [(d, f, 2).into(), (c, d, 1).into()].into_iter(),
    )
    .collect_vec();
    assert_eq!(
        shortest_path_fd_dc,
        vec![(f, d, 2).into(), (d, c, 1).into(), (c, a, 2).into()]
    );
}

/// A tuple-type which is ordered by the first element.
#[derive(Copy, Clone, Debug)]
pub(super) struct Ordered<O, T>(O, T);

impl<O, T> Ord for Ordered<O, T>
where
    O: PartialOrd,
{
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        other.0.partial_cmp(&self.0).unwrap()
    }
}
impl<O, T> PartialOrd for Ordered<O, T>
where
    O: PartialOrd,
{
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        other.0.partial_cmp(&self.0)
    }
}

impl<O, T> Eq for Ordered<O, T> where O: PartialEq {}

impl<O, T> PartialEq for Ordered<O, T>
where
    O: PartialEq,
{
    fn eq(&self, other: &Self) -> bool {
        self.0.eq(&other.0)
    }
}

#[test]
fn test_min_heap() {
    let mut h = BinaryHeap::new();
    h.push(Ordered(2, ()));
    h.push(Ordered(1, ()));
    h.push(Ordered(3, ()));

    assert_eq!(h.pop(), Some(Ordered(1, ())));
    assert_eq!(h.pop(), Some(Ordered(2, ())));
    assert_eq!(h.pop(), Some(Ordered(3, ())));
}

type PrefixTree<NodeId, D> = StableDiGraph<PrefixTreeNode<NodeId, D>, ()>;

pub struct CriticalPaths<G, F, D>
where
    G: GraphBase + Visitable + IntoEdgeReferences,
{
    graph: G,
    edge_cost: F,
    /// Destination node.
    dest: G::NodeId,
    /// Source node.
    start: G::NodeId,
    /// Distance to source and next hop in shortest path for every node in the graph (computed lazily).
    shortest_path_map: Dijkstra<lazy_dijkstra::Precomputed, G, D, F>,
    /// Each node in the prefix tree corresponds to a deviation edge, i.e. an edge
    /// which is not part of a deviation-free shortest path.
    prefix_tree: PrefixTree<G::NodeId, D>,
    /// Root node of the prefix tree.
    prefix_tree_root: NodeIndex,
    /// Priority queue for the next paths.
    /// Each path is implicitly encoded by a node in the prefix tree. Therefore,
    /// This queue only contains tuples of the path cost and the prefix node.
    next_deviation_edges: BinaryHeap<Ordered<D, NodeIndex>>,
    /// Re-used buffer for temporary data. Needed to reuse allocated memory.
    edge_ref_buffer: Vec<(G::EdgeRef, D)>,
}

// #[derive(Clone)] does not work :/. Problem lies in `LazyDijkstra` and <Visitable::Map> not being handled properly by the derive macro.
impl<G, F, D> Clone for CriticalPaths<G, F, D>
where
    G: GraphBase + Visitable + Clone + IntoEdgeReferences,
    F: Clone,
    D: Clone,
    G::Map: Clone,
{
    fn clone(&self) -> Self {
        Self {
            graph: self.graph,
            edge_cost: self.edge_cost.clone(),
            dest: self.dest,
            start: self.start,
            shortest_path_map: self.shortest_path_map.clone(),
            prefix_tree: self.prefix_tree.clone(),
            prefix_tree_root: self.prefix_tree_root,
            next_deviation_edges: self.next_deviation_edges.clone(),
            edge_ref_buffer: Default::default(), // No need to clone it. Used only temporarily.
        }
    }
}

/// Data of a node in the prefix tree.
#[derive(Copy, Clone)]
struct PrefixTreeNode<NodeId, D> {
    /// The deviation from the shortest paths to be taken.
    deviation_edge: PathEdge<NodeId, D>,
    /// Mark the node as visited.
    /// This implies that the corresponding path has been visited.
    // NOTE: can't use the petgraph VisitMap because it does not adapt when nodes are added to the graph.
    visited: bool,
}

impl<N, D> PrefixTreeNode<N, D> {
    fn new(a: N, b: N, cost: D) -> Self {
        Self {
            deviation_edge: PathEdge {
                source: a,
                target: b,
                cost,
            },
            visited: false,
        }
    }
}

/// Iterator over deviation edges.
/// Deviation edges are computed by tracing back to the root node of the prefix tree.
#[derive(Copy, Clone)]
struct DeviationEdges<'a, NodeId, D> {
    prefix_tree: &'a PrefixTree<NodeId, D>,
    /// Root node of the prefix tree.
    prefix_tree_root: NodeIndex,
    /// Start tracing back to root from this node.
    current_prefix_node: NodeIndex,
}

impl<'a, NodeId, D> Iterator for DeviationEdges<'a, NodeId, D>
where
    NodeId: Copy,
    D: Clone,
{
    type Item = PathEdge<NodeId, D>;

    /// Trace back to root node.
    /// Output all nodes on the path, except the root node.
    fn next(&mut self) -> Option<Self::Item> {
        if self.current_prefix_node == self.prefix_tree_root {
            None
        } else {
            let output = self.prefix_tree[self.current_prefix_node]
                .deviation_edge
                .clone();

            // Trace back towards the root.
            self.current_prefix_node = self
                .prefix_tree
                .neighbors_directed(self.current_prefix_node, petgraph::Direction::Incoming)
                .next()
                .expect("a non-root node in a tree must have exactly one parent node");

            Some(output)
        }
    }
}

impl<G, F, D> CriticalPaths<G, F, D>
where
    G: IntoEdgesDirected + Visitable,
    G::NodeId: Hash + Eq + PartialEq,
    D: Measure + Copy,
    F: Fn(G::EdgeRef) -> D,
{
    /// Construct the ordered set of deviation edges from the node in the prefix tree.
    /// Find the deviation edges by backtracking to the root of the prefix tree.
    fn deviation_edges_from_prefix_node(
        &self,
        prefix_node: NodeIndex,
    ) -> DeviationEdges<G::NodeId, D> {
        DeviationEdges {
            prefix_tree: &self.prefix_tree,
            prefix_tree_root: self.prefix_tree_root,
            current_prefix_node: prefix_node,
        }
    }

    /// Construct the path which is implicitly encoded by the prefix node.
    fn path_from_prefix_node(
        &self,
        prefix_node: NodeIndex,
    ) -> impl Iterator<Item = PathEdge<G::NodeId, D>> + Clone + '_ {
        let deviation_edges = self
            .deviation_edges_from_prefix_node(prefix_node)
            .collect_vec() // TODO: avoid allocation
            .into_iter()
            .rev();

        backtrack_with_deviations(
            |n| self.shortest_path_map.next_hop_cached(n),
            self.dest,
            deviation_edges,
        )
    }

    /// Compute the cost of the path which is implicitly encoded by the prefix node.
    /// Compute the cost by summing all edge weights of the path.
    fn path_cost_from_prefix_node_naive(&self, prefix_node: NodeIndex) -> D {
        let deviation_edges = self
            .deviation_edges_from_prefix_node(prefix_node)
            .collect_vec() // TODO: avoid allocation
            .into_iter()
            .rev();

        // Backtrack from this node.
        let backtrack_from = self.dest;

        let path = backtrack_with_deviations(
            |n| self.shortest_path_map.next_hop_cached(n),
            backtrack_from,
            deviation_edges,
        );
        // Compute sum of edge weights.
        path.map(|e| e.cost).fold(D::default(), |a, b| a + b)
    }

    fn next_path(&mut self) -> Option<Vec<PathEdge<G::NodeId, D>>> {
        // Get the deviation edge which will lead to the next critical path.
        // In the first call, this will return the root node of the prefix tree which stands
        // for an empty set of deviation edges.

        // Get set of deviation edges with highest priority.
        let prefix_node = self.next_prefix_node()?;

        if prefix_node == self.prefix_tree_root {
            // First iteration.
            // Do initial shortest path computation.
            self.shortest_path_map.next_hop(&self.dest);
        }

        let path = self.path_from_prefix_node(prefix_node).collect_vec();
        debug_assert!(
            path.is_empty() || path.last().map(|l| l.target) == Some(self.start),
            "backtracking failed"
        );

        if path.is_empty() && self.dest != self.start {
            // No path found.
            return None;
        }

        // Get the longest possible tail of the path which does not contain
        // any deviation edge.
        let (deviated_head, deviation_free_tail) = {
            let tail_len = if prefix_node == self.prefix_tree_root {
                // No deviations taken in the path.
                path.len()
            } else {
                let last_deviation_edge = self.prefix_tree[prefix_node].deviation_edge.reversed(); // The path edges have opposite orientation as the edges in the original graph.

                path.iter()
                    .rev()
                    .take_while(|e| e != &&last_deviation_edge)
                    .count()
            };
            path.split_at(path.len() - tail_len)
        };

        // Cost of deviated head.
        let head_cost = deviated_head
            .iter()
            .map(|e| e.cost)
            .fold(D::default(), |a, b| a + b);

        // Head cost for each additional edge in the tail.
        let head_costs = deviation_free_tail.iter().scan(head_cost, |acc, edge| {
            let prev = *acc;
            *acc = *acc + edge.cost;
            Some(prev)
        });
        debug_assert!(head_cost == head_costs.clone().next().unwrap_or(head_cost));

        // Discover new deviation edges starting from the new path.
        let new_deviation_edges = deviation_free_tail
            .iter()
            .zip(head_costs)
            // For each edge in the path, ...
            .flat_map(|(edge, head_cost)| {
                // ... find possible deviation edges.
                self.graph
                    .edges_directed(edge.source, Direction::Incoming)
                    .inspect(|e| debug_assert!(e.target() == edge.source))
                    // Drop edges which are already in the current path.
                    .filter(|e| e.source() != edge.target)
                    .map(move |e| (e, head_cost))
            })
            // Skip edges which are part of the shortest-path tree.
            .filter(|(e, _head_cost)| {
                let shortest_path_next_hop = self
                    .shortest_path_map
                    .next_hop(&e.target())
                    .unwrap()
                    .next_hop;
                // Return `true` iff the edge `e` is not on a shortest path, i.e. `e` is a deviation edge.
                e.source() != shortest_path_next_hop
            });

        // Write iterator to buffer for reasons of ownership and borrowing.
        let mut buf = std::mem::take(&mut self.edge_ref_buffer); // Take the buffer.
        buf.clear();
        buf.extend(new_deviation_edges);
        let new_deviation_edges = buf.drain(..);

        // Append new deviation edges as leaf nodes to the current node in the prefix tree.
        for (deviation_edge, head_cost) in new_deviation_edges {
            let edge_weight = (self.edge_cost)(deviation_edge);
            let new_leaf = self.prefix_tree.add_node(PrefixTreeNode::new(
                deviation_edge.source(),
                deviation_edge.target(),
                edge_weight,
            ));
            self.prefix_tree.add_edge(prefix_node, new_leaf, ());

            // Cost of shortest path from deviation edge to the source.
            let tail_cost = self
                .shortest_path_map
                .next_hop(&deviation_edge.source())
                .map(|e| e.distance_to_source)
                .unwrap_or_default();

            // Compute cost of path implied by this deviation edge.
            let path_cost = tail_cost + edge_weight + head_cost;
            debug_assert_eq!(path_cost, self.path_cost_from_prefix_node_naive(new_leaf));

            // Put this path in the heap.
            self.next_deviation_edges.push(Ordered(path_cost, new_leaf));
        }

        // Return the buffer for later reuse.
        self.edge_ref_buffer = buf;

        // Path is reversed due to backtracking.
        // Reverse the path.
        let mut path = path;
        path.reverse();
        path.iter_mut().for_each(|e| *e = e.reversed());

        Some(path)
    }

    /// Get the next prefix node from the priority queue.
    /// This node implicitly represents the next critical path.
    fn next_prefix_node(&mut self) -> Option<NodeIndex> {
        loop {
            let Ordered(_priority, n) = self.next_deviation_edges.pop()?;
            // Skip visited nodes.
            let visited = &mut self.prefix_tree[n].visited;
            if !*visited {
                *visited = true;
                break Some(n);
            }
        }
    }
}

#[derive(Copy, Clone)]
struct ImplicitPath<NodeId, Deviations, F> {
    dest: NodeId,
    shortest_path_map: F,
    deviations: Deviations,
}

impl<NodeId, Deviations, F, Dist> IntoIterator for ImplicitPath<NodeId, Deviations, F>
where
    F: FnMut(&NodeId) -> Option<NextHop<Dist, NodeId>>,
    Dist: PartialEq + Clone,
    NodeId: PartialEq + Copy,
    Deviations: Iterator<Item = PathEdge<NodeId, Dist>> + Clone,
{
    type Item = PathEdge<NodeId, Dist>;

    type IntoIter = ImplicitPathIter<NodeId, Deviations, F>;

    fn into_iter(self) -> Self::IntoIter {
        ImplicitPathIter {
            shortest_path_map: self.shortest_path_map,
            deviations: self.deviations.peekable(),
            current_node: self.dest,
        }
    }
}
impl<NodeId, Deviations, F, Dist> ImplicitPath<NodeId, Deviations, F>
where
    F: Fn(&NodeId) -> Option<NextHop<Dist, NodeId>>,
    Dist: PartialEq + Clone,
    NodeId: PartialEq + Copy,
    Deviations: Iterator<Item = PathEdge<NodeId, Dist>> + Clone,
{
    /// Iterate over nodes of the paths.
    fn nodes(self) -> impl Iterator<Item = NodeId> {
        let first = self.dest;
        std::iter::once(first).chain(self.into_iter().map(|e| e.target))
    }
}

#[derive(Clone)]
struct ImplicitPathIter<NodeId, Deviations, F>
where
    Deviations: Iterator,
    Peekable<Deviations>: Clone,
{
    current_node: NodeId,
    shortest_path_map: F,
    deviations: Peekable<Deviations>,
}

impl<NodeId, Dist, F, Deviations> Iterator for ImplicitPathIter<NodeId, Deviations, F>
where
    F: FnMut(&NodeId) -> Option<NextHop<Dist, NodeId>>,
    Dist: PartialEq + Clone,
    NodeId: PartialEq + Copy,
    Deviations: Iterator<Item = PathEdge<NodeId, Dist>> + Clone,
{
    type Item = PathEdge<NodeId, Dist>;

    /// Iterate over edges of the path.
    fn next(&mut self) -> Option<Self::Item> {
        // Test if the current node is adjacent to a deviation edge.
        let (next, cost) = if Some(self.current_node) == self.deviations.peek().map(|e| e.target) {
            // Take the deviation.
            let e = self.deviations.next().unwrap();
            (e.source, e.cost)
        } else {
            // Follow the shortest path.
            if let Some(h) = (self.shortest_path_map)(&self.current_node) {
                (h.next_hop, h.edge_cost)
            } else {
                // Done

                debug_assert!(
                    self.deviations.peek().is_none(), // should be empty
                    "not all deviation edges have been taken: {} remaining",
                    self.deviations.clone().count()
                );

                return None;
            }
        };

        let output = PathEdge {
            source: self.current_node,
            target: next,
            cost,
        };
        self.current_node = next;
        Some(output)
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub struct PathEdge<NodeId, D> {
    source: NodeId,
    target: NodeId,
    cost: D,
}

impl<NodeId, D> PathEdge<NodeId, D> {
    /// Swap source and target nodes.
    fn reversed(self) -> Self {
        Self {
            source: self.target,
            target: self.source,
            cost: self.cost,
        }
    }
}

impl<NodeId, D> From<(NodeId, NodeId, D)> for PathEdge<NodeId, D> {
    fn from(val: (NodeId, NodeId, D)) -> Self {
        PathEdge {
            source: val.0,
            target: val.1,
            cost: val.2,
        }
    }
}

/// Find the path from the destination to the source node. The paths takes all supplied deviation edges
/// and follows the shortest paths segments towards the source otherwise.
/// Deviation edges must be provided in order of appearance in the path.
///
/// # Panics
/// Panics if not all deviation edges are used. This implies that the deviation edges where not correct.
fn backtrack_with_deviations<NodeId, D>(
    shortest_path_map: impl Fn(&NodeId) -> Option<NextHop<D, NodeId>> + Clone,
    dest: NodeId,
    deviations: impl Iterator<Item = PathEdge<NodeId, D>> + Clone,
) -> impl Iterator<Item = PathEdge<NodeId, D>> + Clone
where
    NodeId: PartialEq + Copy,
    D: PartialEq + Clone,
{
    ImplicitPath {
        dest,
        shortest_path_map,
        deviations,
    }
    .into_iter()
}

impl<G, F, D> Iterator for CriticalPaths<G, F, D>
where
    G: IntoEdgesDirected + Visitable,
    G::NodeId: Hash + Eq + PartialEq,
    D: Measure + Copy,
    F: Fn(G::EdgeRef) -> D,
{
    type Item = Vec<PathEdge<G::NodeId, D>>;

    fn next(&mut self) -> Option<Self::Item> {
        self.next_path()
    }
}

#[test]
fn test_critical_path_enumeration_many_paths() {
    // Generate trellis graph.

    let mut g = StableDiGraph::new();
    let s = g.add_node(());
    let d = g.add_node(());

    let n = 8;
    let intermediate_nodes = (0..n)
        .map(|_| (g.add_node(()), g.add_node(())))
        .collect_vec();

    for ((a1, b1), (a2, b2)) in intermediate_nodes
        .iter()
        .zip(intermediate_nodes.iter().skip(1))
    {
        g.add_edge(*a1, *a2, 1);
        g.add_edge(*b1, *b2, 1);
        // Crossing is more expensive.
        g.add_edge(*a1, *b2, 2);
        g.add_edge(*b1, *a2, 2);
    }

    // Connect source and destination.
    g.add_edge(s, intermediate_nodes[0].0, 0);
    g.add_edge(s, intermediate_nodes[0].1, 0);
    g.add_edge(intermediate_nodes[n - 1].0, d, 0);
    g.add_edge(intermediate_nodes[n - 1].1, d, 0);

    let paths = shortest_paths(&g, s, d, |e| *e.weight());

    // Number of paths should be 2^n.
    assert_eq!(paths.count(), 1 << n);
}

#[test]
fn test_circital_paths_empty() {
    let mut g = StableDiGraph::new();
    let a = g.add_node("a");
    let b = g.add_node("b");

    let _e = g.add_edge(a, b, 1);

    // There's no path from b to a because edge is directed the other way.
    let paths = shortest_paths(&g, b, a, |e| *e.weight()).collect_vec();

    dbg!(&paths);
    assert!(paths.is_empty());
}

#[test]
fn test_circital_paths_trivial() {
    let mut g = StableDiGraph::new();
    let a = g.add_node("a");
    let b = g.add_node("b");

    let _e = g.add_edge(a, b, 1);

    // Start and destination are equal.
    let paths = shortest_paths(&g, a, a, |e| *e.weight()).collect_vec();

    assert_eq!(paths, vec![vec![]]);
}

#[test]
fn test_circital_paths_smallest_nontrivial() {
    let mut g = StableDiGraph::new();
    let a = g.add_node("a");
    let b = g.add_node("b");

    let _e = g.add_edge(a, b, 1);

    let paths = shortest_paths(&g, a, b, |e| *e.weight()).collect_vec();

    assert_eq!(
        paths,
        vec![vec![PathEdge {
            source: a,
            target: b,
            cost: 1
        }]]
    );
}

#[test]
fn test_critical_path_enumeration() {
    // Create graph of the form:
    //
    //   /-->b-->d---\
    //  /     \ ^     v
    // a       X       f
    //  \     / v     ^
    //   \-->c-->e---/
    //
    // All edges have weight 1, except ac and df which have weight 2 and 3.
    let mut g = StableDiGraph::new();
    let a = g.add_node("a");
    let b = g.add_node("b");
    let c = g.add_node("c");
    let d = g.add_node("d");
    let e = g.add_node("e");
    let f = g.add_node("f");

    let _ab = g.add_edge(a, b, 1i32);
    let _ac = g.add_edge(a, c, 2);
    let _bd = g.add_edge(b, d, 1);
    let _cd = g.add_edge(c, d, 1);
    let _be = g.add_edge(b, e, 1);
    let _ce = g.add_edge(c, e, 1);
    let _ef = g.add_edge(e, f, 1);
    let _df = g.add_edge(d, f, 3);

    let paths = shortest_paths(&g, a, f, |e| *e.weight());

    // Convert path edges to a list of vertices.
    let path_nodes = |edges: Vec<PathEdge<_, _>>| -> Vec<_> {
        let mut nodes = vec![];
        nodes.extend(edges.first().map(|e| e.source));
        nodes.extend(edges.iter().map(|e| e.target));
        nodes
    };

    {
        // Test internals.
        let mut paths = paths.clone();
        paths.next(); // skip first path
        let prefix_node = paths.next_prefix_node().unwrap();
        assert_eq!(
            paths.prefix_tree[prefix_node].deviation_edge,
            PathEdge {
                source: c,
                target: e,
                cost: 1
            }
        );
        let deviation_edges = paths
            .deviation_edges_from_prefix_node(prefix_node)
            .collect_vec();
        assert_eq!(
            deviation_edges,
            vec![PathEdge {
                source: c,
                target: e,
                cost: 1,
            }]
        );
        assert_eq!(paths.dest, f);
        dbg!(paths.path_from_prefix_node(prefix_node).collect_vec());
    }
    {
        // 1st shortest path
        let nodes = path_nodes(paths.clone().nth(0).unwrap());
        assert_eq!(nodes, vec![a, b, e, f]);
    }

    {
        // 2nd shortest path
        let nodes = path_nodes(paths.clone().nth(1).unwrap());
        assert_eq!(nodes, vec![a, c, e, f],);
    }
    {
        // 3rd shortest path
        let nodes = path_nodes(paths.clone().nth(2).unwrap());
        assert_eq!(nodes, vec![a, b, d, f],);
    }
    {
        // 3rd shortest path
        let nodes = path_nodes(paths.clone().nth(3).unwrap());
        assert_eq!(nodes, vec![a, c, d, f],);
    }

    assert_eq!(paths.count(), 4);
}

#[test]
fn test_longest_path_enumeration() {
    let mut g = StableDiGraph::new();
    let [src, dest, via1, via2] = ["src", "dest", "via1", "via2"].map(|n| g.add_node(n));

    g.add_edge(src, dest, 2);
    g.add_edge(src, via1, 1);
    g.add_edge(via1, via2, 1);
    g.add_edge(via2, dest, 1);

    // Convert path edges to a list of vertices.
    let path_nodes = |edges: Vec<PathEdge<_, _>>| -> Vec<_> {
        let mut nodes = vec![];
        nodes.extend(edges.first().map(|e| e.source));
        nodes.extend(edges.iter().map(|e| e.target));
        nodes
    };

    let paths = longest_paths(&g, src, dest, |e| *e.weight())
        .map(path_nodes)
        .collect_vec();

    assert_eq!(paths, vec![vec![src, via1, via2, dest], vec![src, dest]]);
}

#[test]
fn test_petgraph_intoedges() {
    // Check that for directed graphs the `neighbors` function only outputs nodes connected by an outgoing edge.
    let mut g = StableDiGraph::new();
    let a = g.add_node(());
    let b = g.add_node(());
    let c = g.add_node(());

    g.add_edge(a, b, ());
    g.add_edge(b, c, ());

    let neigbors_of_b: Vec<_> = g.neighbors(b).collect();
    assert_eq!(neigbors_of_b, vec![c]);

    let predecessors_of_b = g
        .edges_directed(b, Direction::Incoming)
        .map(|e| e.source())
        .collect_vec();
    assert_eq!(predecessors_of_b, vec![a]);

    let successors_of_b = g
        .edges_directed(b, Direction::Outgoing)
        .map(|e| e.target())
        .collect_vec();
    assert_eq!(successors_of_b, vec![c]);
}
