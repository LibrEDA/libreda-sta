// SPDX-FileCopyrightText: 2024 Thomas Kramer <code@tkramer.ch>
// SPDX-License-Identifier: AGPL-3.0-or-later

use std::collections::HashMap;

use import::propagation_pattern::EdgeSet;

use crate::traits::cell_logic_model::{BooleanFn, self};

use super::*;

/// Read all lookup table templates in this library group and
/// put them into a `HashMap` indexed by their name.
pub(super) fn read_template_tables(
    lib: &Group,
) -> Result<HashMap<String, LuTableTemplate>, LibertyErr> {
    assert_eq!(
        lib.name, "library",
        "Expected a library group, got '{}'.",
        lib.name
    );
    let mut table_templates = HashMap::new();

    // Process table templates.
    for template in lib.find_groups_by_name("lu_table_template") {
        let template_name = template.arguments.first().ok_or_else(|| {
            log::error!("lu_table_template has no name argument.");
            LibertyErr::InvalidLuTableTemplate("<noname>".to_string())
        })?;

        let variable_1 = template
            .get_simple_attribute("variable_1")
            .and_then(|v| v.as_str())
            .ok_or_else(|| {
                log::error!(
                    "lu_table_template({}): `variable_1` is not defined.",
                    template_name
                );
                LibertyErr::InvalidLuTableTemplate(template_name.to_string())
            })?;
        // variable_2 may not be defined for x*1 sized tables.
        let variable_2 = template
            .get_simple_attribute("variable_2")
            .and_then(|v| v.as_str());

        let index1 = template
            .get_simple_attribute("index_1")
            .and_then(|v| v.as_str())
            .map(liberty_io::util::parse_float_array)
            .ok_or_else(|| {
                log::error!(
                    "`{}` is not well formatted in {}.",
                    "index_1",
                    template_name.to_string()
                );
                LibertyErr::InvalidLuTableTemplate(template_name.to_string())
            })?
            .map_err(|_| {
                log::error!(
                    "`{}` is not well formatted in {}.",
                    "index_1",
                    template_name.to_string()
                );
                LibertyErr::InvalidLuTableTemplate(template_name.to_string())
            })?;

        let index1_size = index1.len();

        let index2_size = if variable_2.is_some() {
            let index2 = template
                .get_simple_attribute("index_2")
                .and_then(|v| v.as_str())
                .map(liberty_io::util::parse_float_array)
                .ok_or_else(|| {
                    log::error!(
                        "`{}` is not well formatted in {}.",
                        "index_2",
                        template_name.to_string()
                    );
                    LibertyErr::InvalidLuTableTemplate(template_name.to_string())
                })?
                .map_err(|_| {
                    log::error!(
                        "`{}` is not well formatted in {}.",
                        "index_2",
                        template_name.to_string()
                    );
                    LibertyErr::InvalidLuTableTemplate(template_name.to_string())
                })?;
            index2.len()
        } else {
            1
        };

        let template = LuTableTemplate {
            var1: variable_1.to_string(),
            var2: variable_2.map(|v| v.to_string()),
            size: (index1_size, index2_size),
        };

        // Store the template.
        table_templates.insert(template_name.to_string(), template);
    }

    Ok(table_templates)
}

impl<CellId, PinId> LibertyTimingLibrary<CellId, PinId>
where
    CellId: Hash + Eq + Clone,
    PinId: Hash + Eq + Clone,
{
    /// Import cells from the liberty data stucture.
    pub(super) fn read_cells(
        &mut self,
        lib: &Group,
        library_context: LibraryImportContext<CellId, PinId>,
    ) -> Result<(), LibertyErr> {
        assert_eq!(lib.name, "library");

        for cell_group in lib.find_groups_by_name("cell") {
            let cell_name = get_cell_name(cell_group)?;

            // Find the cell ID by name. The ID is typically provided by the netlist.
            // Ignore cells which are not present in the netlist.
            let cell_id = if let Some(cell_id) = (library_context.cell_by_name)(cell_name) {
                cell_id
            } else {
                log::info!("Cell not found in netlist (skipped): {}", cell_name);
                continue;
            };

            let mut cell = Cell {
                cell_name: cell_name.into(),
                pins: Default::default(),
                flip_flops: Vec::new(),
                latches: Vec::new(),
                delay_arcs: Default::default(),
            };

            let cell_import_context = CellImportContext {
                library_context: &library_context,
                cell_id: &cell_id,
                cell_name,
            };

            {
                // Import flip-flop group, if any.
                let ff_groups = cell_group
                    .groups
                    .iter()
                    .filter(|g| g.name == "ff" || g.name == "ff_bank");

                for ff_group in ff_groups {
                    let ff = self.read_ff_group(&cell_import_context, ff_group)?;
                    cell.flip_flops.push(ff);
                }
            }

            // Import latch group, if any.
            for latch_group in cell_group.find_groups_by_name("latch") {
                log::warn!("import of latch group not supported yet");
                let latch = self.read_latch_group(&cell_import_context, latch_group)?;
                cell.latches.push(latch);
            }

            // Process all pins.
            for pin_group in cell_group.find_groups_by_name("pin") {
                let (pin_id, pin) = self.read_pin_group(&cell_import_context, &cell, pin_group)?;
                cell.pins.insert(pin_id, pin);
            }

            cell.run_precomputations();
            
            self.cells.insert(cell_id, cell);
        }

        Ok(())
    }

    /// Read a `ff` or `ff_bank` group.
    fn read_ff_group(
        &self,
        cell_context: &CellImportContext<CellId, PinId>,
        ff_group: &Group,
    ) -> Result<FlipFlop<PinId>, LibertyErr> {
        assert!(ff_group.name == "ff" || ff_group.name == "ff_bank");

        // Get name of non-inverted state and inverted state.
        let state_variables = ff_group
            .arguments
            .iter()
            .map(|arg| arg.as_str())
            .collect::<Option<Vec<_>>>()
            .ok_or_else(|| LibertyErr::Other("state variables are not strings".to_string()))?;

        // Get number of storage bits.
        let num_bits = if ff_group.name == "ff" {
            1
        } else {
            assert_eq!(ff_group.name, "ff_bank");
            let num_bits = ff_group
                .arguments
                .get(2)
                .map(|arg| {
                    arg.as_integer().ok_or_else(|| {
                        LibertyErr::Other("failed to parse number of bits".to_string())
                    })
                })
                .inside_out()?
                .ok_or_else(|| {
                    LibertyErr::Other("ff_bank has no 'number of bits' argument".to_string())
                })?;

            // Check that the number of bits is > 0.
            if num_bits < 0 {
                Err(LibertyErr::Other(
                    "number of bits in ff_bank is negative".to_string(),
                ))?;
            }

            num_bits as usize
        };

        // Read boolean function which specifies the clock sensitivity.
        let clocked_on = ff_group
            .get_simple_attribute("clocked_on")
            .map(|expr| Self::parse_boolean_function(expr))
            .inside_out()?
            .as_ref()
            .map(|(expr, var_names)| {
                Self::translate_boolean_function_variables_to_pin_ids(cell_context, expr, var_names)
            })
            .inside_out()?
            .ok_or_else(|| {
                LibertyErr::Other(format!(
                    "ff group has no 'clocked_on' attribute in {}",
                    cell_context.cell_name
                ))
            })?;

        // Read optional second sensitivity.
        let clocked_on_also = ff_group
            .get_simple_attribute("clocked_on_also")
            .map(|expr| Self::parse_boolean_function(expr))
            .inside_out()?
            .as_ref()
            .map(|(expr, var_names)| {
                Self::translate_boolean_function_variables_to_pin_ids(cell_context, expr, var_names)
            })
            .inside_out()?;

        // Read boolean function which specifies the next state of the sequential element.
        // TODO: next state might also depend on the previous state. Currently variable names must correspond to pins.
        let next_state = ff_group
            .get_simple_attribute("next_state")
            .map(|expr| Self::parse_boolean_function(expr))
            .inside_out()?
            .as_ref()
            .map(|(expr, var_names)| {
                Self::translate_boolean_function_variables_to_pin_ids(cell_context, expr, var_names)
            })
            .inside_out()?
            .ok_or_else(|| {
                LibertyErr::Other(format!(
                    "ff group has no 'next_state' attribute in {}",
                    cell_context.cell_name
                ))
            })?;

        Ok(FlipFlop {
            state_variable: state_variables[0].to_string(),
            state_variable_inv: state_variables[1].to_string().to_string(),
            clocked_on,
            clocked_on_also,
            next_state,
            num_bits,
        })
    }

    fn read_latch_group(
        &self,
        cell_context: &CellImportContext<CellId, PinId>,
        latch_group: &Group,
    ) -> Result<Latch<PinId>, LibertyErr> {
        assert_eq!(latch_group.name, "latch");

        // Get name of non-inverted state and inverted state.to_string().
        let state_variables = latch_group
            .arguments
            .iter()
            .map(|arg| arg.as_str().map(|s| s.to_string()))
            .collect::<Option<Vec<_>>>()
            .ok_or_else(|| LibertyErr::Other("state variables are not strings".to_string()))?;

        let data_in = latch_group
            .get_simple_attribute("data_in")
            .map(|expr| Self::parse_boolean_function(expr))
            .inside_out()?
            .as_ref()
            .map(|(expr, var_names)| {
                Self::translate_boolean_function_variables_to_pin_ids(cell_context, expr, var_names)
            })
            .inside_out()?
            .ok_or_else(|| {
                LibertyErr::Other(format!(
                    "latch group has no 'data_in' attribute in {}",
                    cell_context.cell_name
                ))
            })?;

        let enable = latch_group
            .get_simple_attribute("enable")
            .map(|expr| Self::parse_boolean_function(expr))
            .inside_out()?
            .as_ref()
            .map(|(expr, var_names)| {
                Self::translate_boolean_function_variables_to_pin_ids(cell_context, expr, var_names)
            })
            .inside_out()?
            .ok_or_else(|| {
                LibertyErr::Other(format!(
                    "latch group has no 'enable' attribute in {}",
                    cell_context.cell_name
                ))
            })?;

        Ok(Latch {
            state_variable: state_variables[0].to_string(),
            state_variable_inv: state_variables[1].to_string(),
            data_in,
            enable,
        })
    }

    /// Import a pin group from the liberty data structure.
    fn read_pin_group(
        &self,
        cell_import_context: &CellImportContext<CellId, PinId>,
        cell: &Cell<PinId>,
        pin_group: &Group,
    ) -> Result<(PinId, Pin<PinId>), LibertyErr> {
        assert_eq!(pin_group.name, "pin");

        // Get pin name.
        let pin_name = pin_group
            .arguments
            .first()
            .and_then(|v| v.as_str())
            .ok_or_else(|| {
                let msg = format!(
                    "Pin group in cell `{}` has no name argument.",
                    cell_import_context.cell_name
                );
                log::error!("{}", msg);
                LibertyErr::Other(msg)
            })?;

        // Find pin ID by name. The ID is typically provided by the netlist.
        let pin_id = (cell_import_context.library_context.pin_by_name)(
            cell_import_context.cell_id,
            pin_name,
        )
        .ok_or_else(|| {
            LibertyErr::Other(format!(
                "Pin not found in netlist: {}/{}",
                cell_import_context.cell_name, pin_name
            ))
        })?;

        let pin_import_context = PinImportContext {
            cell_context: cell_import_context,
            pin_id: &pin_id,
            pin_name,
        };

        // Get pin capacitance.
        let raw_capacitance = pin_group
            .get_simple_attribute("capacitance")
            .and_then(|v| v.as_float())
            .unwrap_or_default();
        let capacitance = raw_capacitance * self.capacitive_load_unit;
        let capacitance_rise = pin_group
            .get_simple_attribute("rise_capacitance")
            .and_then(|v| v.as_float())
            .unwrap_or(raw_capacitance)
            * self.capacitive_load_unit;
        let capacitance_fall = pin_group
            .get_simple_attribute("fall_capacitance")
            .and_then(|v| v.as_float())
            .unwrap_or(raw_capacitance)
            * self.capacitive_load_unit;

        // Get output function of the pin, if any.
        let output_function = pin_group
            .get_simple_attribute("function")
            .map(|attr| Self::parse_boolean_function(attr))
            .inside_out()?;

        // Find all variable names which refer to sequential states
        // of flip-flops or latches.
        let latch_state_names = {        
            let mut latch_state_names = HashMap::new();
                for latch in &cell.latches {
                    if latch_state_names.insert(&latch.state_variable, latch).is_some() {
                        log::warn!("Duplicate definition of state variable: {}", &latch.state_variable);
                    };
                    if latch_state_names.insert(&latch.state_variable_inv, latch).is_some() {
                        log::warn!("Duplicate definition of state variable: {}", &latch.state_variable_inv);
                    };
                }
            latch_state_names
        };

        let ff_state_names = {        
            let mut ff_state_names = HashMap::new();
                for ff in &cell.flip_flops {
                    if ff_state_names.insert(&ff.state_variable, ff).is_some() {
                        log::warn!("Duplicate definition of state variable: {}", &ff.state_variable);
                    };
                    if ff_state_names.insert(&ff.state_variable_inv, ff).is_some() {
                        log::warn!("Duplicate definition of state variable: {}", &ff.state_variable_inv);
                    };
                }
            ff_state_names
        };
        
        // Check if output function refers to a sequential state.
        let (output_is_latch, output_is_flipflop) = {
          
            // Check if output function refers to a sequential state.
            if let Some((_, var_names)) = &output_function {
                let is_latch = var_names
                    .iter()
                    .any(|var_name| latch_state_names.contains_key(&var_name));
                let is_ff = var_names
                    .iter()
                    .any(|var_name| ff_state_names.contains_key(&var_name));
                (is_latch, is_ff)
            } else {
                // Neither a latch nor a flip-flop.
                (false, false)
            }
        };


        // Sanity check: output cannot be declared as a latch and flip-flop at the same time.
        if output_is_latch && output_is_flipflop {
            log::warn!("Output function of pin {}/{} refers to the state variable of a flip-flop AND a latch. This is not supported.",
            cell_import_context.cell_name, pin_name, 
            );
        }

        // Transform the output function into the required form.
        // Distinguish between combinational and sequential output functions.
        let output_is_sequential = output_is_latch || output_is_flipflop;
        let output_function = if output_is_sequential {

            let mut clocked_on = vec![];
            let mut transparent_on = vec![];
            
            if let Some((f, var_names)) = &output_function {
                // Debug print
                //let f = f.substitute_variable_names(&|idx| &var_names[*idx]);
                //println!("{}", f);

                // Resolve state variables.
                // Find clock-edge conditions.
                clocked_on.extend(
                    var_names.iter()
                    .filter_map(|var_name| ff_state_names.get(var_name))
                    .map(|ff| ff.clocked_on.clone())
                );
                clocked_on.extend(
                    var_names.iter()
                    .filter_map(|var_name| ff_state_names.get(var_name))
                    .filter_map(|ff| ff.clocked_on_also.clone())
                );
                
                // Resolve state variables.
                transparent_on.extend(
                    var_names.iter()
                    .filter_map(|var_name| latch_state_names.get(var_name))
                    .map(|latch| latch.enable.clone())
                );
            }

            // Store the type of sequential circuit (latch, ff, ...)
            OutputFunction::Sequential(
                cell_logic_model::Sequential { 
                    clocked_on, 
                    transparent_on
                }
            )
        } else if let Some((expr, var_names)) = output_function {
            // Translate variable names to PinIds.
            let expr = Self::translate_boolean_function_variables_to_pin_ids(
                cell_import_context,
                &expr,
                &var_names,
            )?;

            OutputFunction::Comb(BooleanFn::Expr(expr))
        } else {
            OutputFunction::Unknown
        };

        let mut pin = Pin {
            capacitance,
            capacitance_rise,
            capacitance_fall,
            output_function,
            pin_name: pin_name.into(),
            timing: Default::default(),
        };

        // Process timing groups.
        for timing_group in pin_group.find_groups_by_name("timing") {
            self.read_timing_group(&mut pin, &pin_import_context, timing_group)?;
        }
        Ok((pin_id, pin))
    }

    /// Parse a value into a boolean expression.
    /// Returns an error if the value is not a string or the string is malformed.
    fn parse_boolean_function(
        function_str: &liberty_io::Value,
    ) -> Result<(BooleanExpr<usize>, Vec<String>), LibertyErr> {
        let output_fn = if let Some(expr_str) = function_str.as_str() {
            // Parse the string into a boolean function.
            parse_boolean_function(expr_str.chars().peekable()).map_err(|_| {
                LibertyErr::Other(format!("Failed to parse boolean function: {}", expr_str))
            })?
        } else {
            // Error: function attribute is not a string.
            return Err(LibertyErr::Other(format!(
                "Failed to parse boolean function. Not a string value: {}",
                function_str
            )));
        };

        Ok(output_fn)
    }

    /// Convert the variable IDs in the boolean expression to `PinId`s with matching names.
    fn translate_boolean_function_variables_to_pin_ids(
        cell_import_context: &CellImportContext<CellId, PinId>,
        expr: &BooleanExpr<usize>,
        variable_names: &[String],
    ) -> Result<BooleanExpr<PinId>, LibertyErr> {
        // Translate variable names into `PinId`s.
        let pin_by_name_fn = |name: &str| {
            (cell_import_context.library_context.pin_by_name)(cell_import_context.cell_id, name)
        };

        // Check that all variable names resolve to existing pins.
        let non_existent_pin_names: Vec<_> = variable_names
            .iter()
            .filter(|name| pin_by_name_fn(name).is_none())
            .collect();

        if !non_existent_pin_names.is_empty() {
            // Error: cannot find pin names which are used in boolean function.
            let msg = format!(
                    "Pin function in cell {} contains variable names which do not correspond to pins in netlist: {}",
                    cell_import_context.cell_name,
                    non_existent_pin_names.iter().join(", ")
                );
            return Err(LibertyErr::Other(msg));
        }

        let expr_with_pinids = expr.substitute_variable_names(&|var_idx| {
            let var_name = variable_names[*var_idx].as_str();

            // Find the pin ID based on the pin name.
            pin_by_name_fn(var_name).expect("existence of pin names should be checked already")
        });

        Ok(expr_with_pinids)
    }

    /// Import a timing group into a `Pin` struct.
    fn read_timing_group(
        &self,
        pin: &mut Pin<PinId>,
        pin_import_context: &PinImportContext<CellId, PinId>,
        timing_group: &Group,
    ) -> Result<(), LibertyErr> {
        assert_eq!(timing_group.name, "timing");

        let cell_name = pin_import_context.cell_context.cell_name;
        let pin_name = pin_import_context.pin_name;
        let cell_id = &pin_import_context.cell_context.cell_id;

        // Get related pin name.
        let related_pin = timing_group
            .get_simple_attribute("related_pin")
            .and_then(|v| v.as_str())
            .ok_or_else(|| {
                log::error!(
                    "Timing group has no related pin in cell `{}`, pin `{}`.",
                    cell_name,
                    pin_name
                );
                LibertyErr::AttributeNotFound("related_pin".to_string())
            })?;

        let related_pin_id =
            (pin_import_context.cell_context.library_context.pin_by_name)(cell_id, related_pin)
                .ok_or_else(|| {
                    LibertyErr::Other(format!(
                        "related pin not found in netlist: {}/{}",
                        cell_name, related_pin
                    ))
                })?;

        let timing_type_str = timing_group
            .get_simple_attribute("timing_type")
            .and_then(|v| v.as_str())
            .unwrap_or("combinational"); // Default is 'combinational'.
        let timing_type = parse_timing_type(timing_type_str, cell_name, pin_name)?;

        let is_delay_arc = matches!(
            timing_type,
            TimingType::Combinational | TimingType::CombinationalRise | TimingType::CombinationalFall 
            | TimingType::RisingEdge | TimingType::FallingEdge | TimingType::Clear | TimingType::Preset
        );

        let is_constraint_arc = matches!(
            timing_type,
            TimingType::HoldRising | TimingType::HoldFalling | TimingType::SetupRising | TimingType::SetupFalling
        );

        // Read a delay arc.
        if is_delay_arc {
            let delay_arc = self.read_delay_arc(cell_name, pin_name, timing_group, timing_type)?;
            // Resolve the input edge types which trigger this delay arc.
            let patterns = PropagationPatterns::new(delay_arc.timing_type, delay_arc.timing_sense);

            let input_edges = [RiseFall::Rise, RiseFall::Fall].into_iter()
            .filter(|&e| {
                let intersection = patterns.intersection(&PropagationPattern::new(e.into(), EdgeSet::RF));
                !intersection.is_empty()
            });

            // Store the delay arc data.
            for edge in input_edges {
                pin.timing.delay_arcs.insert((related_pin_id.clone(), edge), delay_arc.clone());
            }
        }

        // Read a constraint arc.
        if is_constraint_arc {
            let constraint_arc = self.read_constraint_arc(cell_name, pin_name, timing_group)?;
            // Store the constraint arc.
            let (storage, related_edge) = match constraint_arc.timing_type {
                TimingType::HoldRising => (&mut pin.timing.hold_arcs, RiseFall::Rise),
                TimingType::HoldFalling => (&mut pin.timing.hold_arcs, RiseFall::Fall),
                TimingType::SetupRising => (&mut pin.timing.setup_arcs, RiseFall::Rise),
                TimingType::SetupFalling => (&mut pin.timing.setup_arcs, RiseFall::Fall),
                t => unreachable!(
                "Unexpected timing type for constraint arc: {:?}",
                t
                )
            };

            storage.insert((related_pin_id.clone(), related_edge), constraint_arc);
        }

        Ok(())
    }

    /// Parse the look-up tables of a delay arc. This includes combinational arcs and arcs from clock input to data output.
    fn read_delay_arc(
        &self,
        dbg_cell_name: &str, // used for error messages
        dbg_pin_name: &str,  // used for error messages
        timing_group: &Group,
        timing_type: TimingType,
    ) -> Result<DelayArc, LibertyErr> {
        assert_eq!(timing_group.name, "timing");

        let mut delay_arc = DelayArc {timing_type, ..Default::default()};

        // Get unateness.
        let timing_sense_str = timing_group
            .get_simple_attribute("timing_sense")
            .and_then(|v| v.as_str())
            .unwrap_or("non_unate");

        delay_arc.timing_sense = match timing_sense_str {
            "non_unate" => Unateness::None,
            "negative_unate" => Unateness::Negative,
            "positive_unate" => Unateness::Positive,
            t => {
                log::error!("Unknown timing sense in {}/{}: {}", dbg_cell_name, dbg_pin_name, t);
                return Err(LibertyErr::Other(format!("Unknown timing sense: {}", t)));
            }
        };


        // Process NLDM timing tables.
        for table_group in &timing_group.groups {
            // let delay_table_names = ["rise_transition", "fall_transition", "cell_rise", "cell_fall"];
            // let is_delay_arc = delay_table_names.contains(&table_group.name.as_str());

            let required_var1_name = "total_output_net_capacitance";
            let required_var2_name = "input_net_transition";

            let interp = self::import_timing_table::get_interpolated_timing_table(
                timing_group,
                table_group,
                &self.table_templates,
                required_var1_name,
                required_var2_name,
                dbg_cell_name,
                dbg_pin_name,
            )?;

            // Convert units.
            let interp = interp
                .map_values(|&t| t * self.time_unit)
                .map_x_axis(|cap| cap * self.capacitive_load_unit)
                .map_y_axis(|t| t * self.time_unit);

            // Store the table.
            match table_group.name.as_str() {
                "rise_transition" => delay_arc.rise_transition = Some(interp),
                "fall_transition" => delay_arc.fall_transition = Some(interp),
                "cell_rise" => delay_arc.cell_rise = Some(interp),
                "cell_fall" => delay_arc.cell_fall = Some(interp),
                n => {
                    log::warn!("Unsupported timing table: '{}'", n);
                }
            }
        }

        Ok(delay_arc)
    }

    /// Import a constraint arc from a liberty timing group.
    fn read_constraint_arc(
        &self,
        cell_name: &str,
        pin_name: &str,
        timing_group: &Group,
    ) -> Result<ConstraintArc, LibertyErr> {
        assert_eq!(timing_group.name, "timing");

        let timing_type_str = timing_group
            .get_simple_attribute("timing_type")
            .and_then(|v| v.as_str())
            .unwrap_or("combinational"); // Default is 'combinational'.

        let timing_type = parse_timing_type(timing_type_str, cell_name, pin_name)?;

        let mut arc = ConstraintArc  {
            timing_type,
            rise_constraint: Default::default(),
            fall_constraint: Default::default(),
        };

        // Process NLDM timing tables.
        for table_group in &timing_group.groups {
            // let delay_table_names = ["rise_transition", "fall_transition", "cell_rise", "cell_fall"];
            // let constraint_table_names = ["rise_constraint", "fall_constraint"];
            // let is_delay_arc = delay_table_names.contains(&table_group.name.as_str());
            // let is_constraint_arc = constraint_table_names.contains(&table_group.name.as_str());

            let required_var1_name = "related_pin_transition";
            let required_var2_name = "constrained_pin_transition";

            let interp = self::import_timing_table::get_interpolated_timing_table(
                timing_group,
                table_group,
                &self.table_templates,
                required_var1_name,
                required_var2_name,
                cell_name,
                pin_name,
            )?;

            // Convert to SI units.
            let constraint_lut = interp
                .map_values(|&t| t * self.time_unit)
                .map_x_axis(|t| t * self.time_unit)
                .map_y_axis(|t| t * self.time_unit);

            // Store the table.
            match table_group.name.as_str() {
                "rise_constraint" => arc.rise_constraint = Some(constraint_lut),
                "fall_constraint" => arc.fall_constraint = Some(constraint_lut),
                n => {
                    log::warn!("Unsupported timing table: '{}'", n);
                }
            }

        }

        Ok(arc)
    }
}

/// Convert a `Option<Result<T, E>>` into a `Result<Option<T>, E>`.
trait InsideOut<T, E> {
    /// Convert a `Option<Result<T, E>>` into a `Result<Option<T>, E>`.
    fn inside_out(self) -> Result<Option<T>, E>;
}

impl<T, E> InsideOut<T, E> for Option<Result<T, E>> {
    fn inside_out(self) -> Result<Option<T>, E> {
        match self {
            Some(r) => match r {
                Ok(t) => Ok(Some(t)),
                Err(err) => Err(err),
            },
            None => Ok(None),
        }
    }
}


fn parse_timing_type(timing_type: &str, debug_cell_name: &str, debug_pin_name: &str) 
-> Result<TimingType, LibertyErr> {
    match timing_type {
            "combinational" => Ok(TimingType::Combinational),
            "combinational_rise" => Ok(TimingType::CombinationalRise) ,
            "combinational_fall" => Ok(TimingType::CombinationalFall) ,
            "rising_edge" => Ok(TimingType::RisingEdge),
            "falling_edge" => Ok(TimingType::FallingEdge),
            "setup_rising" => Ok(TimingType::SetupRising),
            "setup_falling" => Ok(TimingType::SetupFalling),
            "hold_rising" => Ok(TimingType::HoldRising),
            "hold_falling" => Ok(TimingType::HoldFalling),
            "recovery_rising" => Ok(TimingType::RecoveryRising),
            "recovery_falling" => Ok(TimingType::RecoveryFalling),
            "removal_rising" => Ok(TimingType::RemovalRising),
            "removal_falling" => Ok(TimingType::RemovalFalling),
            "preset" => Ok(TimingType::Preset),
            "clear" => Ok(TimingType::Clear),
            "three_state_disable" => Ok(TimingType::ThreeStateDisable),
            "three_state_enable" => Ok(TimingType::ThreeStateEnable),
            "three_state_disable_rise" => Ok(TimingType::ThreeStateDisableRise),
            "three_state_disable_fall" => Ok(TimingType::ThreeStateDisableFall),
            "three_state_enable_rise" => Ok(TimingType::ThreeStateEnableRise),
            "three_state_enable_fall" => Ok(TimingType::ThreeStateEnableFall),
            t => {
                log::error!("timing_type not supported ({}/{}): {}", debug_cell_name, debug_pin_name, t);
                Err(LibertyErr::Other(format!("timing_type not supported: {}", t)))
            }
        }

}
