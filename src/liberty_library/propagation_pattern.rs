// SPDX-FileCopyrightText: 2024 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use crate::{cell_logic_model::Unateness, RiseFall};

use super::TimingType;

/// Datastructures for representing propagation patterns of delay arcs.
/// For example a delay arc could be sensitive to a rising edge `R` at an input and
/// cause either a rising edge or falling edge (`RF`) at an output. This can be expressed
/// with the [`PropagationPattern`] struct.
///
/// The liberty fileformat specifies the propagation patterns with the `timing_type` and `timing_sense`
/// attributes. Therefore, [`PropagationPatterns`] can be constructed from this two values.

/// Specifies a single mapping from a set of input edge types to a set of output edge types.
/// For example: A logic inverter could have the transition R->F, F->R, RF->RF.
#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash, Default)]
pub struct PropagationPattern {
    /// Transition type of the input signal.
    input_edge: EdgeSet,
    /// Transition type of the output signal.
    output_edge: EdgeSet,
}

/// Specifies two propgation patterns.
#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash, Default)]
pub struct PropagationPatterns {
    patterns: [PropagationPattern; 2],
}

impl PropagationPatterns {
    /// Compute possible propagation patterns.
    /// See Liberty Reference Manual, Table 3-5 "timing_type and timing_sense Values for Combinational Timing Arcs"
    pub fn new(timing_type: TimingType, timing_sense: Unateness) -> Self {
        use EdgeSet::*;
        use TimingType::*;
        use Unateness::*;

        let output_edges = match timing_type {
            Combinational => [R, F],
            CombinationalRise => [R, EdgeSet::None],
            CombinationalFall => [F, EdgeSet::None],
            RisingEdge | FallingEdge | HoldRising | HoldFalling | SetupRising | SetupFalling => {
                [RF, EdgeSet::None]
            }
            Clear => [F, EdgeSet::None],
            Preset => [R, EdgeSet::None],
            _ => [EdgeSet::None, EdgeSet::None],
        };

        let input_edges = match timing_type {
            Combinational | CombinationalFall | CombinationalRise => match timing_sense {
                Unateness::None => output_edges.map(|e| e.union(e.invert())),
                Negative => output_edges.map(|e| e.invert()),
                Positive => output_edges,
            },
            Clear => match timing_sense {
                Unateness::None => [RF, EdgeSet::None],
                Negative => [R, EdgeSet::None],
                Positive => [F, EdgeSet::None],
            },
            Preset => match timing_sense {
                Unateness::None => [RF, EdgeSet::None],
                Negative => [F, EdgeSet::None],
                Positive => [R, EdgeSet::None],
            },
            RisingEdge | SetupRising | HoldRising => [R, EdgeSet::None],
            FallingEdge | SetupFalling | HoldFalling => [F, EdgeSet::None],
            _ => [EdgeSet::None, EdgeSet::None],
        };

        let patterns =
            std::array::from_fn(|i| PropagationPattern::new(input_edges[i], output_edges[i]));

        Self { patterns }.normalized()
    }
    /// Test if the set of patterns contains the other pattern.
    pub fn contains(&self, other: &PropagationPattern) -> bool {
        self.patterns.iter().any(|p| p.contains(other))
    }

    /// Test if this set of patterns contains no other pattern.
    pub fn is_empty(&self) -> bool {
        self.patterns.iter().all(|p| p.is_empty())
    }

    /// Get the set intersection of the patterns.
    pub fn intersection(&self, other: &PropagationPattern) -> Self {
        Self {
            patterns: std::array::from_fn(|i| self.patterns[i].intersection(other)),
        }
        .normalized()
    }

    /// Bring to canonical form for efficient equality check.
    fn normalized(self) -> Self {
        let mut patterns = self.patterns.map(|p| p.normalized());
        // Bring to canoical form.
        patterns.sort_by_key(|p| (-(p.input_edge as isize), -(p.output_edge as isize)));
        Self { patterns }
    }
}

#[test]
fn test_propagationpatterns_intersection() {
    use EdgeSet::*;

    let pr = PropagationPatterns::new(TimingType::CombinationalRise, Unateness::None);
    let pf = PropagationPatterns::new(TimingType::CombinationalFall, Unateness::None);
    let prf = PropagationPatterns::new(TimingType::Combinational, Unateness::None);

    assert_eq!(prf.intersection(&PropagationPattern::new(RF, R)), pr);
    assert_eq!(prf.intersection(&PropagationPattern::new(RF, R)), pr);
    assert_eq!(prf.intersection(&PropagationPattern::new(RF, F)), pf);
}
#[test]
fn test_propagationpatterns_contains() {
    use EdgeSet::*;

    {
        // Inverting arc. Rising and falling output.
        let p = PropagationPatterns::new(TimingType::Combinational, Unateness::Negative);
        assert!(p.contains(&PropagationPattern::new(F, R)));
        assert!(p.contains(&PropagationPattern::new(R, F)));

        assert!(!p.contains(&PropagationPattern::new(R, R)));
        assert!(!p.contains(&PropagationPattern::new(F, F)));
        assert!(!p.contains(&PropagationPattern::new(F, RF)));
        assert!(!p.contains(&PropagationPattern::new(RF, F)));
    }
    {
        // Inverting arc. Rising output only.
        let p = PropagationPatterns::new(TimingType::CombinationalRise, Unateness::Negative);
        assert!(p.contains(&PropagationPattern::new(F, R)));
        assert!(!p.contains(&PropagationPattern::new(R, F)));
    }
    {
        // Non-inverting arc. Rising output only.
        let p = PropagationPatterns::new(TimingType::CombinationalRise, Unateness::Positive);
        assert!(p.contains(&PropagationPattern::new(R, R)));
        assert!(!p.contains(&PropagationPattern::new(R, F)));
        assert!(!p.contains(&PropagationPattern::new(F, F)));
    }
    {
        // Nonunate arc. Rising output only.
        let p = PropagationPatterns::new(TimingType::CombinationalRise, Unateness::None);
        assert!(p.contains(&PropagationPattern::new(F, R)));
        assert!(p.contains(&PropagationPattern::new(R, R)));
        assert!(p.contains(&PropagationPattern::new(RF, R)));
        assert!(!p.contains(&PropagationPattern::new(R, F)));
    }
}

impl PropagationPattern {
    /// Create a new propagation pattern from two sets of edge types.
    pub fn new(input_edge: EdgeSet, output_edge: EdgeSet) -> Self {
        Self {
            input_edge,
            output_edge,
        }
        .normalized()
    }
    /// Construct a propagation pattern from two edge transitions.
    pub fn from_risefall(input: RiseFall, out: RiseFall) -> Self {
        Self::new(input.into(), out.into())
    }

    /// Test if the set of patterns contains the other set of patterns.
    pub fn contains(&self, other: &Self) -> bool {
        self.input_edge.contains(other.input_edge) && self.output_edge.contains(other.output_edge)
    }

    /// Set intersection.
    pub fn intersection(&self, other: &Self) -> Self {
        Self::new(
            self.input_edge.intersection(other.input_edge),
            self.output_edge.intersection(other.output_edge),
        )
    }

    /// Test if this patterns describes no arc at all.
    pub fn is_empty(&self) -> bool {
        self.input_edge == EdgeSet::None || self.output_edge == EdgeSet::None
    }

    /// Bring to canononical form for efficient equality check.
    fn normalized(self) -> Self {
        match (self.input_edge, self.output_edge) {
            (EdgeSet::None, _) | (_, EdgeSet::None) => Self {
                input_edge: EdgeSet::None,
                output_edge: EdgeSet::None,
            },
            _ => self,
        }
    }
}

/// Bitmap of possible signal transition types.
/// R = Rising edge
/// F = Falling edge
/// Z = High impedance
/// L = Low/0
/// H = High/1
/// X = {Low, High}
#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash, Default)]
pub enum EdgeSet {
    /// Empty set.
    #[default]
    None = 0,
    /// Rising edge.
    R = 0b01,
    /// Falling edge.
    F = 0b10,
    /// Rising and falling edge.
    RF = 0b11,
    /// From 1 to Z.
    HZ = 0b0100,
    /// From 0 to Z.
    LZ = 0b1000,
    /// From 0 or 1 to Z.
    XZ = 0b1100,
    /// From Z to 1.
    ZH = 0b010000,
    /// From Z to 0.
    ZL = 0b100000,
    /// From Z to 0 or 1.
    ZX = 0b110000,
}

impl From<RiseFall> for EdgeSet {
    fn from(value: RiseFall) -> Self {
        match value {
            RiseFall::Rise => EdgeSet::R,
            RiseFall::Fall => EdgeSet::F,
        }
    }
}

impl EdgeSet {
    /// Get the edge type which appears by applying a logic inversion to
    /// the original edge type.
    pub fn invert(self) -> Self {
        use EdgeSet::*;
        match self {
            R => F,
            F => R,
            RF => RF,
            HZ => LZ,
            LZ => HZ,
            XZ => XZ,
            ZH => ZL,
            ZL => ZH,
            ZX => ZX,
            None => None,
        }
    }

    /// Compute the set union of the two sets of edges.
    pub fn union(self, other: Self) -> Self {
        Self::from_uint((self as u8) | (other as u8))
    }

    /// Compute the intersection of the two sets of edges.
    pub fn intersection(self, other: Self) -> Self {
        Self::from_uint((self as u8) & (other as u8))
    }

    /// Check if `other` is a subset of `self`.
    pub fn contains(self, other: Self) -> bool {
        (self as usize) & (other as usize) == (other as usize)
    }

    /// Construct the enum from its bitmap representation.
    fn from_uint(u: u8) -> EdgeSet {
        use EdgeSet::*;
        match u {
            0 => None,
            0b000001 => R,
            0b000010 => F,
            0b000011 => RF,
            0b000100 => HZ,
            0b001000 => LZ,
            0b001100 => XZ,
            0b010000 => ZH,
            0b100000 => ZL,
            0b110000 => ZX,
            _ => unreachable!(),
        }
    }
}

#[test]
fn test_edgeset_contains() {
    use EdgeSet::*;
    assert!(EdgeSet::None.contains(EdgeSet::None));
    assert!(!EdgeSet::None.contains(R));

    assert!(RF.contains(F));
    assert!(RF.contains(R));
    assert!(!R.contains(F));
    assert!(!R.contains(RF));
    assert!(!F.contains(RF));

    assert!(ZX.contains(ZL));
    assert!(ZX.contains(ZH));
    assert!(XZ.contains(LZ));
    assert!(XZ.contains(HZ));
}

#[test]
fn test_edgeset_union() {
    use EdgeSet::*;

    assert_eq!(R.union(F), RF);
    assert_eq!(LZ.union(HZ), XZ);
    assert_eq!(ZL.union(ZH), ZX);
}

#[test]
fn test_edgeset_intersection() {
    use EdgeSet::*;
    assert_eq!(R.intersection(F), EdgeSet::None);
    assert_eq!(RF.intersection(F), F);
}
