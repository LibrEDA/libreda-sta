// Copyright (c) 2021-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Define base types used for static timing analysis.

use crate::{traits::LoadBase, RiseFall};

use blanket::blanket;
use libreda_logic::logic_value::Logic3;

/// Defines the concept of signals (e.g.  slew rates and actual arrival times) and output loads (e.g. load capacitance).
#[blanket(derive(Ref))]
pub trait TimingBase: LoadBase {
    /// Representation of signals at input or output pins.
    /// In case of the Non-linear delay model (NLDM) this could be a bundle of the slew rate
    /// and the delay but also the polarity of the signal.
    /// But this type could as well also be a statistical representation of a signal, e.g. a probability
    /// distribution of arrival times.
    type Signal: Signal<LogicValue = Self::LogicValue>;

    /// Type of logic value.
    /// Typically this might be a three-valued type which represents logical `0`, `1` and 'unknown'.
    /// The default is typically 'unknown'.
    /// This is used to specify static input signals when evaluating cell delays or constraints.
    type LogicValue: Copy
        + Clone
        + std::fmt::Debug
        + Default
        + Sync
        + Send
        + From<bool>
        + TryInto<bool>;
}

/// Representation of signals at input or output pins.
//#[blanket(derive(Ref))]
pub trait Signal: Clone + std::fmt::Debug + Sync + Send {
    /// Type of logic value.
    /// Typically this might be a three-valued type which represents logical `0`, `1` and 'unknown'.
    /// The default is typically 'unknown'.
    /// This is used to specify static input signals when evaluating cell delays or constraints.
    type LogicValue: Copy + Clone + std::fmt::Debug + Default + Sync + Send;

    /// Get the target value of a signal.
    fn logic_value(&self) -> Self::LogicValue;

    /// Get the possible type of transition for this signal.
    fn transition_type(&self) -> SignalTransitionType;

    /// Change or specify the edge polarity of the transition.
    // TODO: this should not be necessary, remove it
    fn with_transition_type(self, trans: SignalTransitionType) -> Self;
}

/// Possible types of binary signal transitions.
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum SignalTransitionType {
    /// Signal is at constant (LOW, HIGH or unknown). No transition.
    Constant(Logic3),
    /// Signal transitions from LOW to HIGH.
    Rise,
    /// Signal transitions from HIGH to LOW.
    Fall,
    /// Signal might or might not transition.
    Any,
}

impl From<RiseFall> for SignalTransitionType {
    fn from(value: RiseFall) -> Self {
        match value {
            RiseFall::Rise => Self::Rise,
            RiseFall::Fall => Self::Fall,
        }
    }
}

impl SignalTransitionType {
    /// Transition type after logically inverting the signal.
    pub fn inverted(&self) -> Self {
        use SignalTransitionType::*;
        match self {
            Constant(c) => Constant(!*c),
            Rise => Fall,
            Fall => Rise,
            Any => Any,
        }
    }

    /// Test if the signal is known to have a constant value.
    pub fn is_constant(&self) -> bool {
        matches!(self, Self::Constant(_))
    }
}
