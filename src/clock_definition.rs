// SPDX-FileCopyrightText: 2023 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Datastructures for defining and specifying clock signals.

use num_traits::Zero;

use crate::models::clock_tag::ClockId;

/// Definition of a clock source.
#[derive(Debug, Clone)]
pub struct ClockDefinition<Signal> {
    /// Cycle time of the ideal clock (without any uncertainty).
    pub(crate) period: uom::si::f64::Time,
    /// Representation of the rising edge of a clock cycle.
    /// The `Signal` datatype typically encodes phase/latency and slope.
    /// Might also encode the clock uncertainty.
    pub(crate) rising_edge: Signal,
    /// Representation of the falling edge of a clock cycle.
    /// The `Signal` datatype typically encodes phase/latency and slope.
    /// Might also encode the clock uncertainty.
    pub(crate) falling_edge: Signal,
    pub(crate) clock_id: Option<ClockId>,
}

impl<Signal> ClockDefinition<Signal> {
    /// Create new clock definition.
    /// # Panics
    /// Panics if the period is not a positive normal number.
    pub fn new(period: uom::si::f64::Time, rising_edge: Signal, falling_edge: Signal) -> Self {
        assert!(
            period.is_normal() && period.is_sign_positive() && !period.is_zero(),
            "period must be a positive number but is '{}'",
            period.value
        );
        // TODO: Check that period is larger than delay from rising to falling edge.
        Self {
            period,
            rising_edge,
            falling_edge,
            clock_id: None,
        }
    }
}
