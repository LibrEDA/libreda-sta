// Copyright (c) 2021-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Abstraction of the interconnect delay computation.

use super::delay_base::DelayBase;
use libreda_db::prelude::NetlistBase;
use libreda_db::prelude::TerminalId;

use blanket::blanket;

/// Define the computation of the interconnect delay.
#[blanket(derive(Ref))]
pub trait InterconnectDelayModel<N: NetlistBase>: DelayBase {
    /// Compute the cell delay from the `source_terminal` to the `target_terminal`.
    ///
    /// Returns the output signal at the `target_terminal` or `None` if there is no delay arc from the selected
    /// source to the selected target.
    fn interconnect_output(
        &self,
        netlist: &N,
        source_terminal: &TerminalId<N>,
        input_signal: &Self::Signal,
        target_terminal: &TerminalId<N>,
        output_load: &Self::Load,
    ) -> Option<Self::Signal>;
}
