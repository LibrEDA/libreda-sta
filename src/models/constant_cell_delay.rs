// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Cell model which outputs a constant delay for each cell delay arc.

//use std::marker::PhantomData;
//
//use fnv::FnvHashMap;
//use libreda_db::prelude as db;
//
//use crate::{
//    liberty_library::LibertyTimingLibrary,
//    traits::{CellDelayModel, CellModel, DelayBase, LoadBase, TimingBase},
//};
//
//#[derive(Debug, Copy, Clone)]
//pub struct ConstDelayModel<'a, N: db::NetlistBase, Lib: CellDelayModel<N>> {
//    library: &'a Lib,
//    _netlist_type: PhantomData<N>,
//}
//
//impl<'a, N, Lib> LoadBase for ConstDelayModel<'a, N, Lib>
//where
//    N: db::NetlistBase,
//    Lib: CellDelayModel<N>,
//{
//    type Load = Lib::Load;
//
//    fn sum_loads(&self, load1: &Self::Load, load2: &Self::Load) -> Self::Load {
//        self.library.sum_loads(load1, load2)
//    }
//}
//
//impl<'a, N, Lib> TimingBase for ConstDelayModel<'a, N, Lib>
//where
//    N: db::NetlistBase,
//    Lib: CellDelayModel<N>,
//{
//    type Signal = Lib::Signal;
//
//    type LogicValue = Lib::LogicValue;
//}
//
//impl<'a, N, Lib> DelayBase for ConstDelayModel<'a, N, Lib>
//where
//    N: db::NetlistBase,
//    Lib: CellDelayModel<N>,
//{
//    type Delay = Lib::Delay;
//
//    fn summarize_delays(&self, signal1: &Self::Signal, signal2: &Self::Signal) -> Self::Signal {
//        self.library.summarize_delays(signal1, signal2)
//    }
//
//    fn get_delay(&self, from: &Self::Signal, to: &Self::Signal) -> Self::Delay {
//        self.library.get_delay(from, to)
//    }
//}
//
//impl<'a, N, Lib> CellModel<N> for ConstDelayModel<'a, N, Lib>
//where
//    N: db::NetlistBase,
//    Lib: CellDelayModel<N>,
//{
//    fn ordered_pins(&self, cell: &N::CellId) -> Vec<N::PinId> {
//        self.library.ordered_pins(cell)
//    }
//}
//
//impl<'a, N, Lib> CellDelayModel<N> for ConstDelayModel<'a, N, Lib>
//where
//    N: db::NetlistBase,
//    Lib: CellDelayModel<N>,
//{
//    fn cell_output(
//        &self,
//        netlist: &N,
//        input_pin: &<N as libreda_db::traits::NetlistBase>::PinId,
//        input_signal: &Self::Signal,
//        output_pin: &<N as libreda_db::traits::NetlistBase>::PinId,
//        output_load: &Self::Load,
//        other_inputs: &impl Fn(
//            &<N as libreda_db::traits::NetlistBase>::PinId,
//        ) -> Option<Self::LogicValue>,
//    ) -> Option<Self::Signal> {
//        todo!()
//    }
//
//    fn delay_arcs(
//        &self,
//        netlist: &N,
//        related_pin: &<N as libreda_db::traits::NetlistBase>::PinId,
//    ) -> Box<dyn Iterator<Item = <N as libreda_db::traits::NetlistBase>::PinId> + '_> {
//        self.library.delay_arcs(netlist, related_pin)
//    }
//
//    fn delay_arcs_reversed(
//        &self,
//        output_pin: &N,
//    ) -> Box<dyn Iterator<Item = <N as libreda_db::traits::NetlistBase>::PinId> + '_> {
//        self.library.delay_arcs_reversed(output_pin)
//    }
//}
//
