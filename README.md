<!--
SPDX-FileCopyrightText: 2022 Thomas Kramer

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# LibrEDA STA (Static Timing Analysis)

LibrEDA STA implements static timing analysis algorithms for the LibrEDA framework.
All STA algorithms work on the `NetlistBase` trait and hence are portable
to other netlist data structures.


The concept of timing, delays and constraints is abstracted by a set of traits.
This architecture should allow to implement simple timing models such as the non-linear delay model (NDLM)
and more complicated models (such as statistical models) in a consistent way.

## Documentation

Documentation can be shown with:

```sh
carco doc --open
```

Or alternatively found [here](https://libreda.org/doc/libreda_sta/index.html).
