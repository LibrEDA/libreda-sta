// SPDX-FileCopyrightText: 2024 Thomas Kramer <code@tkramer.ch>
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Constant-time look-up of positions of minimum values in ranges./// Lookup-table based search for the position of the minimum value in a range.
pub struct ArgMinRange<A, T> {
    /// Array on which to perform arg-min lookups.
    array: A,
    lut: Vec<usize>,
    /// (Start, num words) pairs for each window size.
    /// Index j contains the information for window size 2^j.
    sub_lut_indices: Vec<(usize, usize)>,
    _t: std::marker::PhantomData<T>,
}

impl<'a, A, T> ArgMinRange<A, T>
where
    A: AsRef<[T]>,
    T: Ord,
{
    /// Create lookup tables for finding the minimum value in a range and its position.
    /// in constant time.
    /// # Panics
    /// Panics if the array length is 0. Search for a minimum value does not makes sense for emtpy arrays.
    pub fn new(array: A) -> Self {
        assert!(!array.as_ref().is_empty(), "empty lists are not supported");

        let mut new = Self {
            array,
            lut: vec![],
            sub_lut_indices: vec![],
            _t: Default::default(),
        };

        new.allocate_lut();

        new.build_lut();
        new
    }

    /// Get the minimum value in the range in constant time.
    /// If there are multiple minima, return the first.
    pub fn min(&self, start: usize, range_len: usize) -> &T {
        &self.array()[self.argmin(start, range_len)]
    }

    /// Argmin query in constant time.
    /// Find the index of the smallest element in the range `(start..start+range_len)`.
    /// If there are multiple minima, return the first.
    pub fn argmin(&self, start: usize, range_len: usize) -> usize {
        assert!(range_len > 0, "range for argmin query can't be empty");
        assert!(
            start + range_len <= self.array.as_ref().len(),
            "argmin query is out of bounds"
        );

        if range_len == 1 {
            // Trivial case.
            return start;
        }

        // Cover the range with two windows which have a power-of-two length.
        let window_size_log2 = range_len.ilog2();
        let window_size = 1usize << window_size_log2;
        debug_assert!(window_size <= range_len);
        debug_assert!(
            window_size * 2 >= range_len,
            "window should cover at least half of the range"
        );
        let start_left_window = start;
        let start_right_window = start + range_len - window_size;

        let argmin_left = self.get_argmin_in_window(window_size_log2, start_left_window);
        let argmin_right = self.get_argmin_in_window(window_size_log2, start_right_window);

        let value_left = &self.array()[argmin_left];
        let value_right = &self.array()[argmin_right];

        if value_left <= value_right {
            // `<=` becasue we take the first occurrence of the minimum value.
            argmin_left
        } else {
            argmin_right
        }
    }

    /// Access underlying array.
    pub fn array(&self) -> &[T] {
        self.array.as_ref()
    }

    /// Retrieve ownership of the underlying array.
    pub fn take_array(self) -> A {
        self.array
    }

    fn build_lut(&mut self) {
        let largest_window_size_log2 = self.array().len().ilog2();
        let largest_window_size = 1usize << largest_window_size_log2;
        debug_assert!(largest_window_size <= self.array().len());

        for size_log2 in 1..largest_window_size_log2 {
            let window_size = 1 << size_log2;
            let half_window_size = 1 << (size_log2 - 1);
            for window_offset in 0..self.num_windows(window_size) {
                // Use previous results to speed up search.
                let argmin_left = self.argmin(window_offset, half_window_size);
                let argmin_right = self.argmin(window_offset + half_window_size, half_window_size);

                let value_left = &self.array()[argmin_left];
                let value_right = &self.array()[argmin_right];

                let argmin = if value_left <= value_right {
                    argmin_left
                } else {
                    argmin_right
                };
                self.set_argmin_in_window(size_log2, window_offset, argmin);
            }
        }
    }

    /// Get (index into lut, bit offset, bit-length)
    fn get_index(&self, window_size_log2: u32, window_position: usize) -> (usize, u32, u32) {
        let bit_length = window_size_log2;
        let (sub_lut_start, sub_lut_len) = self.sub_lut_indices[window_size_log2 as usize];
        let num_indices_per_word = usize::BITS / bit_length;
        debug_assert!(window_position < sub_lut_len * num_indices_per_word as usize);
        let word_offset = sub_lut_start + window_position / num_indices_per_word as usize;
        let bit_offset = (window_position % num_indices_per_word as usize) * bit_length as usize;

        (word_offset, bit_offset as u32, bit_length)
    }

    fn get_argmin_in_window(&self, window_size_log2: u32, window_start: usize) -> usize {
        let (word_offset, bit_offset, bit_length) = self.get_index(window_size_log2, window_start);

        let offset_relative_to_window =
            Self::get_bits(self.lut[word_offset], bit_offset, bit_length);

        window_start + offset_relative_to_window
    }

    fn set_argmin_in_window(&mut self, window_size_log2: u32, window_start: usize, argmin: usize) {
        debug_assert!(argmin >= window_start);
        let (word_offset, bit_offset, bit_length) = self.get_index(window_size_log2, window_start);

        let offset_relative_to_window = argmin - window_start;
        debug_assert!(offset_relative_to_window <= 1 << window_size_log2);

        let word = &mut self.lut[word_offset];
        *word = Self::set_bits(*word, offset_relative_to_window, bit_offset, bit_length);

        // Retreive and check value which we just stored.
        debug_assert_eq!(
            argmin,
            self.get_argmin_in_window(window_size_log2, window_start)
        );
    }

    /// Extract bits from an integer.
    fn get_bits(word: usize, bit_offset: u32, bit_length: u32) -> usize {
        let mask = (1 << bit_length) - 1;
        (word >> bit_offset) & mask
    }

    /// Set bits int `word` to the specified `value`.
    fn set_bits(word: usize, value: usize, bit_offset: u32, bit_length: u32) -> usize {
        debug_assert!(value < 1 << bit_length);
        let mask = (1 << (bit_length + bit_offset)) - (1 << bit_offset);

        (word & !mask) | (value << bit_offset)
    }

    fn allocate_lut(&mut self) {
        let largest_window_size_log2 = self.array().len().ilog2();

        // Compute start indices of the look-up tables for each window size.
        let window_sizes_log2 = 0..largest_window_size_log2 + 1;
        let mut sub_lut_indices = Vec::with_capacity(window_sizes_log2.len());
        let mut total_lut_len = 0;
        for s_log2 in window_sizes_log2 {
            let num_words = self.required_storage_for_window_size(s_log2, usize::BITS as usize);
            sub_lut_indices.push((total_lut_len, num_words));
            total_lut_len += num_words;
        }
        self.sub_lut_indices = sub_lut_indices;

        let lut = vec![0; total_lut_len];

        self.lut = lut;
    }

    /// Get number of words needed to store the indices of the smalles elements in all windows
    /// of the given size. Indices are stored relatively to the start of the window.
    /// Therefore, less bits are used for short windows.
    fn required_storage_for_window_size(
        &self,
        window_size_log2: u32,
        word_size_bits: usize,
    ) -> usize {
        let total_bits = Self::num_bits_for_offset(window_size_log2) as usize
            * self.num_windows(1usize << window_size_log2);
        // Round up
        (total_bits + word_size_bits - 1) / word_size_bits
    }

    /// Get number of bits used to encode the position relative to the window.
    fn num_bits_for_offset(window_size_log2: u32) -> u32 {
        window_size_log2.next_power_of_two()
    }

    /// Get the number of sliding windows which fit into the array to be searched.
    fn num_windows(&self, window_size: usize) -> usize {
        assert!(window_size <= self.array().len());
        self.array().len() - window_size + 1
    }

    fn sub_lut_index(&self, start: usize, range_len: usize) -> usize {
        debug_assert!(range_len > 1);
        debug_assert!(range_len.is_power_of_two());
        debug_assert!(start + range_len <= self.array().len());
        todo!()
    }
}

#[test]
#[should_panic]
fn test_argmin_range_empty_array() {
    let list: Vec<isize> = vec![];
    let _am = ArgMinRange::new(&list);
}

#[test]
fn test_argmin_range_pass_ownership() {
    let list: Vec<isize> = vec![1, 2, 3];
    let _am = ArgMinRange::new(list);
}

#[test]
fn test_argmin_range_random_exhaustive() {
    use itertools::Itertools;

    let prng = |i| i * 17 + (i * 1234567 % 61);

    for len in 1..20 {
        // Create pseudo-random values.
        let list = (0..len).map(prng).collect_vec();

        let am = ArgMinRange::new(&list);

        assert_eq!(am.array.len(), list.len());

        // Test exhaustively.
        for len in 1..list.len() {
            for start in 0..list.len() - len {
                assert_eq!(
                    am.argmin(start, len),
                    list[start..start + len].iter().position_min().unwrap() + start
                );
            }
        }
    }
}
