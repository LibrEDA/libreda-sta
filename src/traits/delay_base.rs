// Copyright (c) 2021-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Abstraction of the delay computation.

use num_traits::Zero;

use super::timing_base::TimingBase;

use blanket::blanket;

/// Abstraction of a delay model.
#[blanket(derive(Ref))]
pub trait DelayBase: TimingBase {
    /// Type representing a delay.
    /// This can be as simple as a `f64` or more complicated such as a probability distribution.
    type Delay: Clone + std::fmt::Debug + Zero + Send + Sync;

    /// Summarize multiple possible output signals into one signal.
    /// Depending on the timing analysis mode (late/early) this might be
    /// a `max` or `min` function.
    // TODO: Rename to 'summarize_signals'
    fn summarize_delays(&self, signal1: &Self::Signal, signal2: &Self::Signal) -> Self::Signal;

    /// Compute the delay from one signal to another signal.
    fn get_delay(&self, from: &Self::Signal, to: &Self::Signal) -> Self::Delay;
}
