// SPDX-FileCopyrightText: 2023 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Trait for a cell library which provides logic views of the cells.

use liberty_io::boolean::BooleanExpr;
use libreda_db::traits::NetlistIds;
use libreda_logic::truth_table::small_lut::SmallTruthTable;

use super::CellModel;

use blanket::blanket;

/// Either a combinational/boolean function or a state-holding element.
#[derive(Debug, Clone, Default)]
#[non_exhaustive]
pub enum OutputFunction<PinId> {
    /// The output function of the pin is not known.
    #[default]
    Unknown,
    /// Combinational logic: The value of the output pin is defined by a boolean function.
    Comb(BooleanFn<PinId>),
    /// Sequential logic: The value of the output pin is defined by a state-holding element.
    Sequential(Sequential<PinId>),
}

/// Description of an output pin which is driven by a sequential circuit.
#[derive(Clone, PartialEq, Eq, Hash, Debug)]
pub struct Sequential<PinId> {
    /// The sequential output is updated when at least one
    /// of the boolean expressions transitions from `false` to `true`.
    pub clocked_on: Vec<BooleanExpr<PinId>>,
    /// TODO: Latches:
    pub transparent_on: Vec<BooleanExpr<PinId>>,
    //TODO pub data: BooleanExpr<PinOrState<PinId>>,
    //TODO pub next_state: BooleanExpr<PinOrState<PinId>>,
}

/// A boolean input which can either be an input pin
/// or an internal state. Used for storage elements such as flip-flops
/// and latches.
#[derive(Copy, Clone, PartialEq, Eq, Hash, Debug)]
pub enum PinOrState<PinId> {
    /// An input pin.
    Pin(PinId),
    /// An internal state bit.
    State(usize),
}

/// Unateness describes a monotonic property of a boolean function.
/// Usually, unateness is related to a specific input.
#[derive(Copy, Clone, PartialEq, Eq, Hash, Debug, Default)]
pub enum Unateness {
    /// A rising/falling edge at the input can cause both rising and falling edges at the output.
    #[default]
    None,
    /// An edge at the input can only cause edges of the opposite polarity at the output.
    Negative,
    /// An edge at the input can only cause edges of the same polarity at the output.
    Positive,
}

/// Representation of a boolean function.
#[derive(Debug, Clone)]
#[non_exhaustive]
pub enum BooleanFn<PinId> {
    /// Logic function with few inputs (6 or less).
    Small(SmallTruthTable),
    /// A boolean expression tree.
    Expr(BooleanExpr<PinId>),
}

/// Provide logic abstraction of digital cells.
#[blanket(derive(Ref))]
pub trait LogicModel<N: NetlistIds>: CellModel<N> {
    /// Get the logic function of a cell output pin.
    fn pin_function(&self, output_pin: &N::PinId) -> &OutputFunction<N::PinId>;

    /// Get the unateness of the timing arc.
    fn timing_sense(
        &self,
        output_pin: &N::PinId,
        related_pin: &N::PinId,
        other_inputs: &impl Fn(&N::PinId) -> Option<bool>,
    ) -> Unateness;
}
